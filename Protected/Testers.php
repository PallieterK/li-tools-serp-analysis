<?php
	function NDCGLog ($curDocIDs, $prevDocIDs)
	{
		$cur = array();
		$cur = explode(',', $curDocIDs);
		$prev = array();
		$prev = explode(',', $prevDocIDs);
		
		$curId2Pos = array ();
		$prevIdToPos = array ();
		$allId = array ();
		
		foreach ($cur as $pos => $id) {
			$curId2Pos [$id] = $pos;
			$allId []= $id;
		}

		foreach ($prev as $pos => $id) {
			$prevId2Pos [$id] = $pos;
			$allId []= $id;
		}

		$allId = array_unique ($allId);

		$sum = 0;
		$nP = count ($allId);

		$sumMax = 0;

		foreach ($allId as $id) {

			if (array_key_exists ($id, $prevId2Pos) 
			    && array_key_exists ($id, $curId2Pos)
			    ) {
				$rel = abs ($curId2Pos [$id] - $prevId2Pos [$id]) / $nP / 2;
				$pos = floor (($curId2Pos [$id] + $prevId2Pos [$id]) / 2);
				
			} else if (array_key_exists ($id, $prevId2Pos)) {
				$rel = 0.5;
				$pos = $prevId2Pos [$id];
				
			} else if (array_key_exists ($id, $curId2Pos)) {
				$rel = 0.5;
				$pos = $curId2Pos [$id];
			}

			$d = $nP - $pos + 1;
			
			$sum += $rel / log (2, $d);
			$sumMax += 0.5 / log (2, $d);
		}

		return ($sum / $sumMax) * 100;
	}

	function NDCGPow ($curDocIDs, $prevDocIDs)
	{
		$cur = array();
		$cur = explode(',', $curDocIDs);
		$prev = array();
		$prev = explode(',', $prevDocIDs);

		$curId2Pos = array ();
		$prevIdToPos = array ();
		$allId = array ();
		
		foreach ($cur as $pos => $id) {
			$curId2Pos [$id] = $pos;
			$allId []= $id;
		}

		foreach ($prev as $pos => $id) {
			$prevId2Pos [$id] = $pos;
			$allId []= $id;
		}

		$allId = array_unique ($allId);

		$sum = 0;
		$nP = count ($allId);

		foreach ($allId as $id) {

			if (array_key_exists ($id, $prevId2Pos) 
			    && array_key_exists ($id, $curId2Pos)
			    ) {
				$rel = abs ($curId2Pos [$id] - $prevId2Pos [$id]) ? 0.5 : 0;
				$pos = floor (($curId2Pos [$id] + $prevId2Pos [$id]) / 2);
				
			} else if (array_key_exists ($id, $prevId2Pos)) {
				$rel = 0.5;
				$pos = $prevId2Pos [$id];
				
			} else if (array_key_exists ($id, $curId2Pos)) {
				$rel = 0.5;
				$pos = $curId2Pos [$id];
			}
			
			if ($pos > 1) {
				$d = $pos;
			} else {
				$d = 1;
			}
			
			$sum += $rel / pow (2, $d);
		}

		return ($sum / 2) * 100;
	}

	function Spearman ($curDocIDs, $prevDocIDs)
	{
		$cur = array();
		$cur = explode(',', $curDocIDs);
		$prev = array();
		$prev = explode(',', $prevDocIDs);

		$curId2Pos = array ();
		$prevIdToPos = array ();
		$allId = array ();
		
		foreach ($cur as $pos => $id) {
			$curId2Pos [$id] = $pos;
			$allId []= $id;
		}

		foreach ($prev as $pos => $id) {
			$prevId2Pos [$id] = $pos;
			$allId []= $id;
		}

		$allId = array_unique ($allId);

		$sum = 0;
		$nP = count ($allId);
		$mP = max (count ($curId2Pos), count ($prevId2Pos));

		foreach ($allId as $id) {

			if (array_key_exists ($id, $prevId2Pos) 
			    && array_key_exists ($id, $curId2Pos)
			    ) {
				$d = $curId2Pos [$id] - $prevId2Pos [$id];
				
			} else if (array_key_exists ($id, $prevId2Pos)) {
				$d = $mP - $prevId2Pos [$id];
				
			} else if (array_key_exists ($id, $curId2Pos)) {
				$d = $mP - $curId2Pos [$id];
			}
			
			$sum += $d * $d;
		}

		$nP++;

		$r = 1 - (6 * $sum) / ($nP * ($nP*$nP-1));

		return ($r) * 100;
	}