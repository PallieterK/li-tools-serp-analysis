<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$Action = (empty($_GET['Action']) ? '' : trim(strip_tags(substr($_GET['Action'], 0, 64)))); // XSS prevention.
	if (!empty($_SESSION['UserID']) && !empty($_SESSION['AdminLevel']))
	{
		if (!empty($Action) && file_exists(realpath($HomeDir.'/Protected/Modules/Admin/'.$Action.'.inc')))
		{
			if ($_SESSION['AdminLevel'] == 'admin')
			{
				require(realpath($HomeDir.'/Protected/Modules/Admin/'.$Action.'.inc'));
			}
			else
			{
				echo '<p>Sorry, no access.</p>';
			}
		}
		elseif (!empty($Action))
		{
			echo getWord($Action);
		}
		else
		{
			echo '<h1>Content</h1><ul>';
			$AdminModules = scandir(dirname(__FILE__).'/Admin/');
			foreach ($AdminModules as $Key => $FileName)
			{
				if (substr($FileName, -4) == '.inc')
				{
					echo '<li><a class="AdminLink" href="?Page=Admin&amp;Action='.substr($FileName, 0, -4).'">'.getWord('Menu', substr($FileName, 0, -4)).'</a></li>';
				}
			}
			echo '</ul>';
		}
	}
	else
	{
		echo '<p>Access not allowed or login was expired.</p>';
		echo '<p>Go to the <a href="'.basename($_SERVER['PHP_SELF']).'?Page=Login">login page</a>.</p>';
	}
?>