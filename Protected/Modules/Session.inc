<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$Debug = false;
	$MaxDocIDsStored = 100;
	$TryMax = 3; // How many times we will try to query the API if a request failed (no understandable response).

	$Action = (empty($_GET['Action']) ? '' : trim(strip_tags(substr($_GET['Action'], 0, 64)))); // XSS prevention.
	$Task = (empty($_GET['Task']) ? '' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
	$DryRun = ((isset($_GET['DryRun']) && $_GET['DryRun'] == 'true') ? true : false); // If true, it will not store any data in the database.
	$IncludeAllEnvironmentsTest = ((isset($_GET['IncludeAllEnvironmentsTest']) && $_GET['IncludeAllEnvironmentsTest'] == 'true') ? true : false); // If true, it will include all the EnvironmentID=NULL tests as well.

	$TestID = (empty($_REQUEST['TestID']) || !is_numeric($_REQUEST['TestID']) ? 0 : $_REQUEST['TestID']); // XSS prevention.
	$EnvironmentID = (empty($_REQUEST['EnvironmentID']) || !is_numeric($_REQUEST['EnvironmentID']) ? 0 : $_REQUEST['EnvironmentID']); // XSS prevention.
	$SessionID = (!empty($_GET['SessionID']) && is_numeric($_GET['SessionID']) ? $_GET['SessionID'] : ''); // XSS prevention.

	if (false)
	{
		echo '<pre>';
		$Result = get_defined_vars();
		var_dump($Result);
		echo '</pre>';
	}

	echo '<h1>Session</h1>';

	if (!empty($TestID) || !empty($EnvironmentID))
	{
		if (empty($TestID))
		{
			// Run a session for the requested environment:
			$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments WHERE ID='.$EnvironmentID;
			$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$EnvironmentRow = mysql_fetch_assoc($EnvironmentRes);
			// Loop all the active tests:
			$TestStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Tests WHERE EnvironmentID='.$EnvironmentRow['ID'];
			if ($IncludeAllEnvironmentsTest)
			{
				$TestStr .= ' OR EnvironmentID IS NULL';
			}
		}
		else
		{
			$TestStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Tests WHERE ID='.$TestID;
		}
		$TestRes = mysql_query($TestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$TestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$TestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$TestNum = mysql_num_rows($TestRes);
		if (empty($TestNum))
		{
			echo '<p class="Error">There are no tests for this environment, <a href="?Page=Test&amp;Task=New&amp;EnvironmentID='.$EnvironmentID.'">create some</a>.</p>';
		}
		else
		{
			// Log this run:
			$SessionStr = 'INSERT INTO '.$DataBase['TablePrefix'].'Sessions SET ';
			if (!empty($TestRow['EnvironmentID']))
			{
				 $SessionStr .= 'EnvironmentID = '.$TestRow['EnvironmentID'].', ';
			}
			elseif (!empty($EnvironmentID))
			{
				 $SessionStr .= 'EnvironmentID = '.$EnvironmentID.', ';
			}
			if (empty($TestID))
			{
				$SessionStr .= 'TestID=0, ';
			}
			else
			{
				$SessionStr .= 'TestID='.$TestID.', ';
			}
			if (!empty($_REQUEST['SessionDescription']))
			{
				 $SessionStr .= "Description = '".mysql_real_escape_string(urldecode($_REQUEST['SessionDescription']))."', ";
			}
			$TimerOverallStart = date('Y-m-d H:i:s');
			$SessionStr .= " DateStart='".$TimerOverallStart."';";
			if ($DryRun)
			{
				$SessionID = 0;
			}
			else
			{
				$SessionRes = mysql_query($SessionStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
				if ($Debug) { echo 'MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				$SessionID = mysql_insert_id();
			}
			$AllTestsSucceeded = true;

			// Loop all tests:
			while ($TestRow = mysql_fetch_assoc($TestRes))
			{
				// In the case only 1 specific test was requested, we'll get the Environment data here:
				if (empty($EnvironmentRow) && !empty($TestRow['EnvironmentID']))
				{
					$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments WHERE ID='.$TestRow['EnvironmentID'];
					$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
					if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
					$EnvironmentRow = mysql_fetch_assoc($EnvironmentRes);
				}
				elseif (empty($EnvironmentRow) && empty($TestRow['EnvironmentID']))
				{
					$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments WHERE DateEnd IS NULL OR DateEnd > NOW()';
					$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
					if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
					$EnvironmentRow = mysql_fetch_assoc($EnvironmentRes);
				}
				// Get the results:
				$URL = str_replace('[[QUERY]]', urlencode($TestRow['SearchQuery']), $EnvironmentRow['API_URL']);
				$Timer = -microtime(true); // Timer: START
				$Try = 0;
				do
				{
					sleep($Try); // TODO: to be fair, we should reduce this wait time from $Timer.
					$Try++;
					$Result = @file_get_contents($URL);
					//var_dump($Result);
				}
				while ($Try < $TryMax && empty($Result));
				$Timer += microtime(true); // Timer: END
				if (empty($Result))
				{
				 	echo '<p class="Error">The API on environment <a href="?Page=Environment&amp;Task=Edit&amp;EnvironmentID='.$TestRow['EnvironmentID'].'">'.$TestRow['EnvironmentID'].'</a> did not respond (with content) after '.$Try.' requests to <a href="'.$URL.'">this</a> URL for TestID <a href="?Page=Test&amp;Task=Edit&amp;TestID='.$TestRow['ID'].'">'.$TestRow['ID'].'</a>. You can <a href="?Page=Session&TestID='.$TestRow['ID'].'">try this test again</a>.</p>';
				 	$AllTestsSucceeded = false;
				}
				else
				{
					$TestPassed = true;
					$Value = '';
					//if ($Debug) { var_dump(htmlspecialchars($Result)); }
					libxml_use_internal_errors(true);
					$XML = simplexml_load_string($Result);
					if ($XML === false)
					{
						echo '<p class="Error">The returned XML is not valid, see: ';
						foreach (libxml_get_errors() as $Error)
						{
							echo '<br />'.$Error->message;
						}
						echo '</p>';
						$AllTestsSucceeded = false;
					}
					$ErrosReportedInXML = $XML->xpath('/searchRetrieveResponse/diagnostics/diagnostic/x-key');
					$numberOfRecordsObject = $XML->xpath('/searchRetrieveResponse/numberOfRecords');
					$numberOfRecords = intval($numberOfRecordsObject[0]);
					if (!empty($ErrosReportedInXML))
					{
						// TODO: if a total failure is experienced, lik wrong password, stop further test processing.

						if ($ErrosReportedInXML[0] == 'NoRecordsFound')
						{
							echo '<p class="Error">The API returned no results (reason: NoRecordsFound).</p>';
						}
						else
						{
							echo '<p class="Error">The API returned an unknown error: '.$ErrosReportedInXML[0].'.</p>';
							$AllTestsSucceeded = false;
						}
					}
					elseif (empty($numberOfRecords))
					{
						echo '<p class="Error">The returned XML does not contain any results.</p>';
						$AllTestsSucceeded = false;
					}
					else
					{
						$DocIDsArray = array();
						$DocIDNodes = $XML->xpath('/searchRetrieveResponse/records/record/recordData/dc/ID');
						//var_dump($DocIDNodes);
						while (list(, $DocumentID) = each($DocIDNodes))
						{
							$DocIDsArray[] = $DocumentID;
						}
						//var_dump($DocIDsArray);
						// Process the result, check the test type:
						// TODO: ResultDocumentTitle
						// TODO: ResultPositionRangeEnd
						// TODO: ResultPositionRangeStart
						if (!empty($TestRow['ResultDocumentID']))
						{
							if (!in_array($TestRow['ResultDocumentID'], $DocIDsArray))
							{
								$TestPassed = false;
							}
						}
						if (!empty($TestRow['ResultCountRangeStart']))
						{
							if ($numberOfRecords < $TestRow['ResultCountRangeStart'])
							{
								$TestPassed = false;
							}
						}
						if ($TestRow['ResultCountRangeEnd'])
						{
							if ($numberOfRecords > $TestRow['ResultCountRangeEnd'])
							{
								$TestPassed = false;
							}
						}
						if (!$TestPassed) { $AllTestsSucceeded = false; }
						// Write the results:
						$ResultStr = 'INSERT INTO '.$DataBase['TablePrefix'].'Results SET SessionID = '.$SessionID.', TestID = '.$TestRow['ID'].', ';
						$ResultStr .= " TestResult='".$TestPassed."', ";
						$ResultStr .= " Value='".mysql_real_escape_string($Value)."', ";
						$ResultStr .= " Output='".mysql_real_escape_string($Result)."', ";
						$DocIDs = '';
						for ($i = 0; $i < $MaxDocIDsStored && $i < count($DocIDsArray); $i++)
						{
							$DocIDs .= $DocIDsArray[$i].',';
						}
						$DocIDs = substr($DocIDs, 0, -1);
						$ResultStr .= " DocIDs='".$DocIDs."', "; // Note: for now, we only track the first 100 results.
						$ResultStr .= " DocCount='".$numberOfRecords."', ";
						$ResultStr .= "DateStart='".date('Y-m-d H:i:s', $Time)."', Duration=".$Timer.";";
						if ($DryRun)
						{
							//echo 'This is a DryRun, otherwise we would have done: '.$ResultStr;
						}
						else
						{
							$ResultRes = mysql_query($ResultStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$ResultStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
							if ($Debug) { echo 'MySQL Query: '.$ResultStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
						}
					}
					echo '<p>The test (ID: <a href="?Page=Test&amp;Task=Edit&amp;TestID='.$TestRow['ID'].'">'.$TestRow['ID'].'</a>) took '.$Timer.' seconds and we tried '.$Try.' time(s).</p>'."\n";
				}
				// Push the test result to the user (for impatient people and also to keep the connection open).
				flush();
				ob_flush();
			}
			// Log the end time for this session:
			$TimerOverallEnd = date('Y-m-d H:i:s');
			$SessionStr = 'UPDATE '.$DataBase['TablePrefix']."Sessions SET Result = ".($AllTestsSucceeded ? 1 : 0).", DateEnd='".$TimerOverallEnd."' WHERE ID = ".$SessionID.";";
			if (!$DryRun)
			{
				$SessionRes = mysql_query($SessionStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
				if ($Debug) { echo 'MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			}
			$TimerOverallSeconds = strtotime($TimerOverallEnd) - strtotime($TimerOverallStart);
			if ($DryRun)
			{
				echo '<p>The session took: '.($TimerOverallSeconds).' seconds (that is: '.round($TimerOverallSeconds/60, 1).' minutes) for '.$TestNum.' test(s) and the overall result was '.($AllTestsSucceeded ? '' : '<strong>NOT</strong>').' successful.</p>'."\n";
				echo '<p>Note: this was a Dry Run, if you want to run the test again and store the results in the database, <a href="?Page=Session&amp;EnvironmentID='.$EnvironmentRow['ID'];
				if (!empty($_REQUEST['SessionDescription']))
				{
					echo '&amp;SessionDescription='.urlencode($_REQUEST['SessionDescription']);
				}
				echo '&amp;DryRun=false&amp;IncludeAllEnvironmentsTest='.($IncludeAllEnvironmentsTest ? 'true' : 'false').'">click here</a>.';
			}
			else
			{
				echo '<p>The session (ID: <a href="?Page=Session&amp;Task=View&amp;SessionID='.$SessionID.'">'.$SessionID.'</a>) took: '.($TimerOverallSeconds).' seconds (that is: '.round($TimerOverallSeconds/60, 1).' minutes) for '.$TestNum.' test(s) and the overall result was '.($AllTestsSucceeded ? '' : '<strong>NOT</strong>').' successful.</p>'."\n";
			}

		}
	}
	elseif (!empty($Task) && $Task == 'View')
	{
		$SessionStr = 'SELECT E.ID AS EnvironmentID, S.ID AS SessionsID, E.Name AS EnvironmentsName, E.Description AS EnvironmentDescription, S.DateStart AS SessionsDateStart, S.DateEnd AS SessionsDateEnd, S.Result AS SessionsResult, S.Description AS SessionName FROM '.$DataBase['TablePrefix'].'Sessions AS S JOIN '.$DataBase['TablePrefix'].'Environments AS E ON S.EnvironmentID = E.ID WHERE ';
		if (empty($SessionID) && !empty($EnvironmentID))
		{
			// Show a latest test result for this environment:
			$SessionStr .= 'E.ID = '.$EnvironmentID.' AND S.DateEnd IS NOT NULL ORDER BY S.ID DESC LIMIT 1';
		}
		elseif (!empty($SessionID))
		{
			// Show a specific test result:
			$SessionStr .= 'S.ID = '.$SessionID;
		}
		elseif (empty($SessionID) && empty($EnvironmentID))
		{
			// Show a latest test result:
			$SessionStr .= 'S.DateEnd IS NOT NULL ORDER BY S.ID DESC LIMIT 1';
		}
		else
		{
			exit('f817db8b-9981-4300-8d89-361d0ae3e37a');
		}
		$SessionRes = mysql_query($SessionStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$SessionStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$SessionRow = mysql_fetch_assoc($SessionRes);
		echo 'This "'.$SessionRow['SessionName'].'" session (ID: '.$SessionRow['SessionsID'].') from the "'.$SessionRow['EnvironmentsName'].'" environment "'.$SessionRow['EnvironmentDescription'].'" (ID: '.$SessionRow['EnvironmentID'].') was '.($SessionRow['SessionsResult'] ? '' : '<strong>NOT</strong> ').'fully successful.<br />';
		echo 'Session duration: '.(strtotime($SessionRow['SessionsDateEnd']) - strtotime($SessionRow['SessionsDateStart'])).' seconds. It was started on '.$SessionRow['SessionsDateStart'].' and ended on '.$SessionRow['SessionsDateEnd'].'.<br />';

		if (!empty($EnvironmentID))
		{
			$CurrentEnvironmentID = $EnvironmentID;
		}
		elseif (!empty($SessionRow['EnvironmentID']))
		{
			$CurrentEnvironmentID = $SessionRow['EnvironmentID'];
		}
		else
		{
			exit('738970d9-42a7-4fcc-9f75-4c93f5f51454'); // Should never happen.
		}
		// Compare test durations for this environment:
		$HystoricalSessionHTML = '';
		$SparklineDurationValuesArray = array();
		$AllPreviousResultsStr = 'SELECT S.DateStart, S.DateEnd, S.Description, S.ID FROM '.$DataBase['TablePrefix'].'Sessions AS S WHERE EnvironmentID = '.$CurrentEnvironmentID.' ORDER BY S.DateStart DESC';
		$AllPreviousResultsRes = mysql_query($AllPreviousResultsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AllPreviousResultsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$AllPreviousResultsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		while ($AllPreviousResultsRow = mysql_fetch_assoc($AllPreviousResultsRes))
		{
			$SparklineDurationValuesArray[] = strtotime($AllPreviousResultsRow['DateEnd']) - strtotime($AllPreviousResultsRow['DateStart']);
			if ($SessionRow['SessionsID'] != $AllPreviousResultsRow['ID'])
			{
				$HystoricalSessionHTML .= '<li>'.$AllPreviousResultsRow['Description'].' ('.$AllPreviousResultsRow['ID'].')</li>';
			}
		}
?>
					<script type="text/javascript">
					    $(function()
					    {
						$('#ISL-Duration').sparkline(
							[0, <?php echo implode(',', $SparklineDurationValuesArray); ?>], { type: 'bar' });
					    });
					</script>
<?php
		echo 'Historical test durations for this environment (G: 0, a-z): <span class="inlinesparkline" id="ISL-Duration" ></span><br />';

		if (!empty($HystoricalSessionHTML))
		{
			echo '<p>This report includes the following hystorical sessions:<ul>'.$HystoricalSessionHTML.'</ul></p>';
		}

		echo '<p>The test results are:</p>';
		$TestsStr = 'SELECT R.*, T.Name, T.Description, T.SearchQuery FROM '.$DataBase['TablePrefix'].'Results AS R JOIN '.$DataBase['TablePrefix'].'Tests AS T ON R.TestID = T.ID WHERE R.SessionID = '.$SessionRow['SessionsID'].' ORDER BY T.Category ASC, T.Name ASC, R.TestID DESC';
		$TestsRes = mysql_query($TestsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		include_once($HomeDir.'Protected/Testers.php');
		// Display the test results:
		//echo '<script type="text/javascript"> $(document).ready(function() { $("#SessionResultTable").DataTable(); } ); </script>';
		echo '<table border="1" id="SessionResultTable" border="1" cellspacing="0">';
		echo '<tr><th>Test<br /><br />ID: Name (or Query)<br />Description</th><th>Duration<br />Prev<br />Cur<br />G (0, a-z)</th><th>Value<br /><br />Prev<br />Cur</th><th>Doc Count<br />Prev<br />Cur<br />G (0, a-z)</th><th>NDCG Log<br /><br />Cur<br />G (0, a-z)</th><th>NDCG Pow<br /><br />Cur<br />G (0, a-z)</th><th>Spearman<br /><br />Cur<br />G (0, a-z)</th></tr>';
		while ($TestsRow = mysql_fetch_assoc($TestsRes))
		{
			$PreviousTestStr = 'SELECT R.* FROM '.$DataBase['TablePrefix'].'Results AS R JOIN '.$DataBase['TablePrefix'].'Sessions AS S ON R.SessionID = S.ID WHERE S.EnvironmentID = '.$CurrentEnvironmentID.' AND R.ID < '.$TestsRow['ID'].' AND R.TestID = '.$TestsRow['TestID'].' ORDER BY R.DateStart DESC LIMIT 1';
			$PreviousTestRes = mysql_query($PreviousTestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$PreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$PreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$PreviousTestNum = mysql_num_rows($PreviousTestRes);
			if (empty($PreviousTestNum))
			{
				unset($PreviousTestRow);
				$PreviousTestRow = array();
			}
			else
			{
				$PreviousTestRow = mysql_fetch_assoc($PreviousTestRes);
			}
			unset($ValuesSparklines);
			$ValuesSparklines = array();
			$AllPreviousTestStr = 'SELECT R.DocIDs, R.Duration, R.DocCount FROM '.$DataBase['TablePrefix'].'Results AS R JOIN '.$DataBase['TablePrefix'].'Sessions AS S ON R.SessionID = S.ID WHERE S.EnvironmentID = '.$CurrentEnvironmentID.' AND R.ID <= '.$TestsRow['ID'].' AND R.TestID = '.$TestsRow['TestID'].' ORDER BY R.DateStart ASC';
			$AllPreviousTestRes = mysql_query($AllPreviousTestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AllPreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$AllPreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$AllPreviousTestNum = mysql_num_rows($AllPreviousTestRes);
			if (empty($AllPreviousTestNum))
			{
				$AllPreviousTestRow = array();
			}
			else
			{
				while ($AllPreviousTestRow = mysql_fetch_assoc($AllPreviousTestRes))
				{
					$ValuesSparklines['DocIDs'][] = $AllPreviousTestRow['DocIDs'];
					if (!empty($AllPreviousTestRow['DocCount']))
					{
						$ValuesSparklines['DocIDsCount'][] = $AllPreviousTestRow['DocCount'];
					}
					$ValuesSparklines['Duration'][] = $AllPreviousTestRow['Duration'];
				}
				//var_dump($ValuesSparklines); exit();
			}

			echo '<tr'.(empty($TestsRow['TestResult']) ? ' style="background-color: #FF0000"' : '').'><td>';
				echo '<a href="?Page=Test&amp;Task=Edit&amp;TestID='.$TestsRow['TestID'].'">'.$TestsRow['TestID'].'</a>: '.($TestsRow['Name'] ? $TestsRow['Name'] : htmlspecialchars($TestsRow['SearchQuery'])).'<br />';
				echo $TestsRow['Description'].'<br />';
			echo '</td><td>';
				if (!empty($PreviousTestRow['Duration']))
				{
					echo number_format($PreviousTestRow['Duration'], 2).'<br />';
				}
				else
				{
					echo '&nbsp;<br />';
				}
				echo number_format($TestsRow['Duration'], 2).'<br />';
				if (!empty($ValuesSparklines['Duration']))
				{
?>
					<script type="text/javascript">
					    $(function()
					    {
						$('#ISL-Duration-<?php echo $TestsRow['TestID']; ?>').sparkline(
							[0, <?php echo implode(',', $ValuesSparklines['Duration']); ?>], { type: 'bar' });
					    });
					</script>
<?php
					echo '<span class="inlinesparkline" id="ISL-Duration-'.$TestsRow['TestID'].'" ></span>';
				}
			echo '</td><td>';
				if (!empty($PreviousTestRow)) //  && $TestsRow['Value'] != $PreviousTestRow['Value']
				{
					echo $PreviousTestRow['Value'].'<br />';
				}
				else
				{
					echo '&nbsp;<br />';
				}
				echo $TestsRow['Value'].'<br />';
			echo '</td><td>';
				if (!empty($PreviousTestRow['DocCount']))
				{
					echo $PreviousTestRow['DocCount'].'<br />';
				}
				else
				{
					echo '&nbsp;<br />';
				}
				echo $TestsRow['DocCount'].'<br />';
				if (!empty($ValuesSparklines['DocIDsCount']))
				{
					$ValuesSparklinesDocIDsCount = '';
					foreach($ValuesSparklines['DocIDsCount'] AS $ValuesSparklinesDocIDsCountValue) { $ValuesSparklinesDocIDsCount .= $ValuesSparklinesDocIDsCountValue.','; };
					$ValuesSparklinesDocIDsCount = substr($ValuesSparklinesDocIDsCount, 0, -1);
?>
					<script type="text/javascript">
						$(function()
						{
							$('#ISL-DocIDsCount-<?php echo $TestsRow['TestID']; ?>').sparkline([0, <?php echo $ValuesSparklinesDocIDsCount; ?>], { type: 'bar' });
						});
					</script>
<?php
					echo '<span class="inlinesparkline" id="ISL-DocIDsCount-'.$TestsRow['TestID'].'"></span>';
				}
			echo '</td><td>';
				if (!empty($TestsRow['DocIDs']) && !empty($PreviousTestRow['DocIDs']))
				{
					echo number_format(NDCGLog($TestsRow['DocIDs'], $PreviousTestRow['DocIDs']), 4, '.', '');
				}
				if (!empty($ValuesSparklines['DocIDs']))
				{
?>
					<script type="text/javascript">
						$(function()
						{
							$('#ISL-NDCGLog-<?php echo $TestsRow['TestID']; ?>').sparkline([0, <?php for ($i = 1, $VSLDocIDsCount = count($ValuesSparklines['DocIDs']); $i < $VSLDocIDsCount; $i++) { echo NDCGLog($ValuesSparklines['DocIDs'][$i], $ValuesSparklines['DocIDs'][$i-1]).','; }; ?>], { type: 'bar' });
						});
					</script>
<?php
					echo '<br /><span class="inlinesparkline" id="ISL-NDCGLog-'.$TestsRow['TestID'].'"></span>';
				}
			echo '</td><td>';
				if (!empty($TestsRow['DocIDs']) && !empty($PreviousTestRow['DocIDs']))
				{
					echo number_format(NDCGPow($TestsRow['DocIDs'], $PreviousTestRow['DocIDs']), 4, '.', '');
				}
				if (!empty($ValuesSparklines['DocIDs']))
				{
?>
					<script type="text/javascript">
						$(function()
						{
							$('#ISL-NDCGPow-<?php echo $TestsRow['TestID']; ?>').sparkline([0, <?php for ($i = 1, $VSLDocIDsCount = count($ValuesSparklines['DocIDs']); $i < $VSLDocIDsCount; $i++) { echo NDCGPow($ValuesSparklines['DocIDs'][$i], $ValuesSparklines['DocIDs'][$i-1]).','; }; ?>], { type: 'bar' });
						});
					</script>
<?php
					echo '<br /><span class="inlinesparkline" id="ISL-NDCGPow-'.$TestsRow['TestID'].'"></span>';
				}
			echo '</td><td>';
				if (!empty($TestsRow['DocIDs']) && !empty($PreviousTestRow['DocIDs']))
				{
					echo number_format(Spearman($TestsRow['DocIDs'], $PreviousTestRow['DocIDs']), 4, '.', '');
				}
				if (!empty($ValuesSparklines['DocIDs']))
				{
?>
					<script type="text/javascript">
						$(function()
						{
							$('#ISL-Spearman-<?php echo $TestsRow['TestID']; ?>').sparkline([0, <?php for ($i = 1, $VSLDocIDsCount = count($ValuesSparklines['DocIDs']); $i < $VSLDocIDsCount; $i++) { echo Spearman($ValuesSparklines['DocIDs'][$i], $ValuesSparklines['DocIDs'][$i-1]).','; }; ?>], { type: 'bar' });
						});
					</script>
<?php
					echo '<br /><span class="inlinesparkline" id="ISL-Spearman-'.$TestsRow['TestID'].'"></span>';
				}
			echo '</td></tr>';
		}
		echo '</table>';
	}
	else
	{
		echo '<p class="Error">Access not allowed or the requested action/task is unknown.</p>';
	}
?>