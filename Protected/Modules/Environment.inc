<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$EnvironmentID = (empty($_REQUEST['EnvironmentID']) || !is_numeric($_REQUEST['EnvironmentID']) ? 0 : $_REQUEST['EnvironmentID']); // XSS prevention.
//	$Task = (empty($_GET['Task']) ? 'New' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
	$Action = (!empty($EnvironmentID) ? 'Edit' : (empty($_GET['Action']) ? 'List' : trim(strip_tags(substr($_GET['Action'], 0, 64))))); // XSS prevention.
	
         
        echo '<h1>Environments</h1>';
         
        switch ($Action)
	{
		case 'Add':
			// Add the Environment to the database:
			if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
			{
				exit('Spammer?');
			}
			$AddEnvironmentStr = 'INSERT INTO '.$DataBase['TablePrefix'].'Environments SET ';
			if (!empty($_POST['Name']))
			{
				$AddEnvironmentStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
			}
			if (!empty($_POST['Description']))
			{
				$AddEnvironmentStr .= "Description='".mysql_real_escape_string($_POST['Description'])."', ";
			}
			if (!empty($_POST['API_URL']))
			{
				$AddEnvironmentStr .= "API_URL='".mysql_real_escape_string($_POST['API_URL'])."', ";
			}
			if (!empty($_POST['API_Version']))
			{
				$AddEnvironmentStr .= "API_Version='".mysql_real_escape_string($_POST['API_Version'])."', ";
			}
			if (!empty($_POST['DateStart']))
			{
				$AddEnvironmentStr .= "DateStart='".mysql_real_escape_string($_POST['DateStart'])."', ";
			}
			if (!empty($_POST['DateEnd']))
			{
				$AddEnvironmentStr .= "DateEnd='".mysql_real_escape_string($_POST['DateEnd'])."', ";
			}
			$AddEnvironmentStr .= "DateCreated='".date('Y-m-d H:i:s', $Time)."', Created_By_UserID=".$_SESSION['UserID'].";";
			$AddEnvironmentRes = mysql_query($AddEnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AddEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
			if ($Debug) { echo 'MySQL Query: '.$AddEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$EnvironmentID = mysql_insert_id();
			echo '<p>De Environment is opgeslagen (ID: '.$EnvironmentID.'). Pas de Environment hieronder aan, of <a href="?Page=Admin&amp;Action=Environment">voeg een nieuwe Environment toe.</a></p>';
		case 'Edit':
			if (empty($EnvironmentID))
			{
				echo '<p class="Error">Edit request failed; no (valid) EnvironmentID.</p>';
			}
			else
			{
				// Show the edit form:
				$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments WHERE ID='.$EnvironmentID;
				$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
				if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				$EnvironmentRow = mysql_fetch_assoc($EnvironmentRes);
				echo '<form id="EditEnvironmentForm" action="';
				if (!empty($Page)) { echo '?Page='.$Page; }
				if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
                               
				echo '&amp;Task=Update" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
				echo '<table>';
				echo '<tr><td>Environments ID</td><td>'.$EnvironmentRow['ID'].'</td></tr>';
				echo '<tr><td>Name</td><td><select name="Name"><option value="dev"'.($EnvironmentRow['Name'] == 'dev' ? ' selected="selected"' : '').'>dev</option><option value="test"'.($EnvironmentRow['Name'] == 'test' ? ' selected="selected"' : '').'>test</option><option value="acceptance"'.($EnvironmentRow['Name'] == 'acceptance' ? ' selected="selected"' : '').'>acceptance</option><option value="production"'.($EnvironmentRow['Name'] == 'production' ? ' selected="selected"' : '').'>production</option></select></td></tr>';
				echo '<tr><td>Description</td><td><input type="text" name="Description" value="'.htmlspecialchars($EnvironmentRow['Description']).'" /></td></tr>';
				echo '<tr><td>API_URL</td><td><input type="text" name="API_URL" value="'.htmlspecialchars($EnvironmentRow['API_URL']).'" /></td></tr>';
				echo '<tr><td>API_Version</td><td><input type="text" name="API_Version" value="'.htmlspecialchars($EnvironmentRow['API_Version']).'" /></td></tr>';
				echo '<tr><td>DateStart</td><td><input type="text" name="DateStart" value="'.htmlspecialchars($EnvironmentRow['DateStart']).'" /></td></tr>';
				echo '<tr><td>DateEnd</td><td><input type="text" name="DateEnd" value="'.htmlspecialchars($EnvironmentRow['DateEnd']).'" /></td></tr>';
				echo '<tr><td>Date Created</td><td>'.$EnvironmentRow['DateCreated'].'</td></tr>';
				echo '<tr><td>Created_By_UserID</td><td>'.$EnvironmentRow['Created_By_UserID'].'</td></tr>';
				echo '<tr><td>Last Updated</td><td>'.$EnvironmentRow['DateUpdated'].'</td></tr>';
				echo '<tr><td>Last Updated By UserID</td><td>'.$EnvironmentRow['Updated_By_UserID'].'</td></tr>';
				$_SESSION['SpamCheck'] = rand();
				echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /><input type="hidden" name="EnvironmentID" value="'.$EnvironmentRow['ID'].'" /></td><td><input type="submit" name="submit" value="SAVE" /></td></tr>';
				echo '</table>';
			}
			break;
		case 'Update':
			// Update the editted Environment:
			if (!empty($_POST['EnvironmentID']) && is_numeric($_POST['EnvironmentID']))
			{
				if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
				{
					exit('Spammer?');
				}
				// Environment:
				$EditEnvironmentStr = 'UPDATE '.$DataBase['TablePrefix'].'Environments SET ';
				if (!empty($_POST['Name']))
				{
					$EditEnvironmentStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
				}
				if (!empty($_POST['Description']))
				{
					$EditEnvironmentStr .= "Description='".mysql_real_escape_string($_POST['Description'])."', ";
				}
				if (!empty($_POST['API_URL']))
				{
					$EditEnvironmentStr .= "API_URL='".mysql_real_escape_string($_POST['API_URL'])."', ";
				}
				if (!empty($_POST['API_Version']))
				{
					$EditEnvironmentStr .= "API_Version='".mysql_real_escape_string($_POST['API_Version'])."', ";
				}
				if (!empty($_POST['DateStart']))
				{
					$EditEnvironmentStr .= "DateStart='".mysql_real_escape_string($_POST['DateStart'])."', ";
				}
				if (!empty($_POST['DateEnd']))
				{
					$EditEnvironmentStr .= "DateEnd='".mysql_real_escape_string($_POST['DateEnd'])."', ";
				}
				$EditEnvironmentStr .= "DateUpdated='".date('Y-m-d H:i:s', $Time)."', Updated_By_UserID = ".$_SESSION['UserID']." WHERE ID=".$_POST['EnvironmentID'].";";
				$EditEnvironmentRes = mysql_query($EditEnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EditEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
				if ($Debug) { echo 'MySQL Query: '.$EditEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				echo '<p>De Environment is aangepast. ';
				echo 'Terug naar de <a href="?Page=Admin">admin</a>.</p>';
			}
			else
			{
				echo '<p class="Error">Update request failed; no (valid) EnvironmentID.</p>';
			}
			break;
		case 'New':
			// Show the Add Environment form:
			echo '<form id="AddEnvironmentForm" action="';
			if (!empty($Page)) { echo '?Page='.$Page; }
			if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
			echo '&amp;Task=Add" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
			echo '<table>';
			echo '<tr><td>Name</td><td><select name="Name"><option value="dev">dev</option><option value="test">test</option><option value="acceptance">acceptance</option><option value="production">production</option></select></td></tr>';
			echo '<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>';
			echo '<tr><td>API_URL</td><td><input type="text" name="API_URL" /></td></tr>';
			echo '<tr><td>API_Version</td><td><input type="text" name="API_Version" /></td></tr>';
			echo '<tr><td>DateStart</td><td><input type="text" name="DateStart" /></td></tr>';
			echo '<tr><td>DateEnd</td><td><input type="text" name="DateEnd" /></td></tr>';
			$_SESSION['SpamCheck'] = rand();
			echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" name="submit" value="'.getWord('Forms', 'Submit').'" /></td></tr>';
			echo '</table>';
			break;
		case 'List':
			// List all existing Environments:
			echo '<div id="NewItem"><a href="?Page=Environment&amp;Action=New">Create a new environment...</a></div>';
			echo 'TODO: create the list view';
/*** sample code to be modified:
		echo '<form id="UsersSearchForm" action="';
		if (!empty($Page)) { echo '?Page='.$Page; }
		if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
		echo '&amp;Task=List" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
		echo '<table>';
		echo '<tr><td>Environment</td><td><select name="EnvironmentID">';
		echo '<option value="ALL"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'ALL' ? ' selected="selected"' : '').'>search all tests</option>';
		echo '<option value="NULL"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'NULL' ? ' selected="selected"' : '').'>search all-environments tests</option>';
			$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments ORDER BY Name ASC, API_URL ASC, API_Version DESC';
			$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
			{
				echo '<option value="'.$EnvironmentRow['ID'].'"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == $EnvironmentRow['ID'] ? ' selected="selected"' : '').'>'.$EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'].'</option>';
			}
		echo '</select></td></tr>';
		echo '<tr><td>Name</td><td><input type="text" name="Name"'.(!empty($_POST['Name']) ? ' value="'.htmlspecialchars($_POST['Name']).'"' : '').' /></td></tr>';
		echo '<tr><td>Category</td><td><input type="text" name="Category"'.(!empty($_POST['Category']) ? ' value="'.htmlspecialchars($_POST['Category']).'"' : '').' /></td></tr>';
		echo '<tr><td>Description</td><td><input type="text" name="Description"'.(!empty($_POST['Description']) ? ' value="'.htmlspecialchars($_POST['Description']).'"' : '').' /></td></tr>';
		echo '<tr><td>Priority</td><td><input type="text" name="Description"'.(!empty($_POST['Weighting']) ? ' value="'.htmlspecialchars($_POST['Weighting']).'"' : '').' /></td></tr>';
		echo '<tr><td>SearchQuery</td><td><input type="text" name="SearchQuery"'.(!empty($_POST['SearchQuery']) ? ' value="'.htmlspecialchars($_POST['SearchQuery']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultDocumentID</td><td><input type="text" name="ResultDocumentID"'.(!empty($_POST['XResultDocumentIDXXX']) ? ' value="'.htmlspecialchars($_POST['ResultDocumentID']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultDocumentTitle</td><td><input type="text" name="ResultDocumentTitle"'.(!empty($_POST['ResultDocumentTitle']) ? ' value="'.htmlspecialchars($_POST['ResultDocumentTitle']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultPositionRangeStart</td><td><input type="text" name="ResultPositionRangeStart"'.(!empty($_POST['ResultPositionRangeStart']) ? ' value="'.htmlspecialchars($_POST['ResultPositionRangeStart']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultPositionRangeEnd</td><td><input type="text" name="ResultPositionRangeEnd"'.(!empty($_POST['ResultPositionRangeEnd']) ? ' value="'.htmlspecialchars($_POST['ResultPositionRangeEnd']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultCountRangeStart</td><td><input type="text" name="ResultCountRangeStart"'.(!empty($_POST['ResultCountRangeStart']) ? ' value="'.htmlspecialchars($_POST['ResultCountRangeStart']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultCountRangeEnd</td><td><input type="text" name="ResultCountRangeEnd"'.(!empty($_POST['ResultCountRangeEnd']) ? ' value="'.htmlspecialchars($_POST['ResultCountRangeEnd']).'"' : '').' /></td></tr>';
		if (!empty($_SESSION['SpamCheck'])) { $SpamCheck = $_SESSION['SpamCheck']; }
		$_SESSION['SpamCheck'] = rand();
		echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" value="'.getWord('Forms', 'Search').'" name="submit" /></td></tr>';
		echo '</table><br />';
		// Show the list of available Users:
		$TestsStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Tests ';
		if (!empty($_POST) && $SpamCheck == $_POST['SpamCheck'])
		{
			$Where = false;
			$TestsWhereStr = '';
			if (!empty($_POST['EnvironmentID']) && is_numeric($_POST['EnvironmentID']))
			{
				$TestsWhereStr .= "EnvironmentID = '".$_POST['EnvironmentID']."' AND ";
				$Where = true;
			}
			if (!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'NULL')
			{
				$TestsWhereStr .= "EnvironmentID IS NULL ____";
				$Where = true;
			}
			if (!empty($_POST['Name']))
			{
				$TestsWhereStr .= "Name LIKE '%".mysql_real_escape_string($_POST['Name'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['Category']))
			{
				$TestsWhereStr .= "Category LIKE '%".mysql_real_escape_string($_POST['Category'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['Description']))
			{
				$TestsWhereStr .= "Description LIKE '%".mysql_real_escape_string($_POST['Description'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['Weighting']) && is_numeric($_POST['Weighting']))
			{
				$TestsWhereStr .= "Weighting  >= ".$_POST['Weighting']." AND ";
				$Where = true;
			}
			if (!empty($_POST['SearchQuery']))
			{
				$TestsWhereStr .= "SearchQuery='".mysql_real_escape_string($_POST['SearchQuery'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultDocumentID']) && is_numeric($_POST['ResultDocumentID']))
			{
				$TestsWhereStr .= "ResultDocumentID = '".mysql_real_escape_string($_POST['ResultDocumentID'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultDocumentTitle']))
			{
				$TestsWhereStr .= "ResultDocumentTitle LIKE '%".mysql_real_escape_string($_POST['ResultDocumentTitle'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultPositionRangeStart']) && is_numeric($_POST['ResultPositionRangeStart']))
			{
				$TestsWhereStr .= "ResultPositionRangeStart = '".mysql_real_escape_string($_POST['ResultPositionRangeStart'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultPositionRangeEnd']) && is_numeric($_POST['ResultPositionRangeEnd']))
			{
				$TestsWhereStr .= "ResultPositionRangeEnd = '".mysql_real_escape_string($_POST['ResultPositionRangeEnd'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultCountRangeStart']) && is_numeric($_POST['ResultCountRangeStart']))
			{
				$TestsWhereStr .= "ResultCountRangeStart = '".mysql_real_escape_string($_POST['ResultCountRangeStart'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultCountRangeEnd']) && is_numeric($_POST['ResultCountRangeEnd']))
			{
				$TestsWhereStr .= "ResultCountRangeEnd = '".mysql_real_escape_string($_POST['ResultCountRangeEnd'])."' AND ";
				$Where = true;
			}

			if ($Where)
			{
				$TestsStr .= 'WHERE '.substr($TestsWhereStr, 0, -4);
			}
		}
		$TestsStr .= 'ORDER BY Name';
		$TestsRes = mysql_query($TestsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$TestsNum = mysql_num_rows($TestsRes);
		if (empty($TestsNum))
		{
			echo '<p class="Warning">No tests available in the system.</p>';
		}
		else
		{
			// Use this jQuery to make the whole row clickable:
?>
			<script>
				google.setOnLoadCallback(function()
				{
					$('#TestList tr').click(function()
					{
						var href = $(this).find('a').attr('href');
						if (href)
						{
							window.location = href;
						}
					});
				});
			</script>
<?php
			$RowCounter=0;
			echo '<table id="TestList">'."\n";
			echo '<tr><th>ID</th><th>EnvironmentID</th><th>Name</th><th>Category</th><th>Description</th><th>Weighting</th><th>SearchQuery</th><th>SearchParameters</th><th>ResultDocumentID</th><th>ResultDocumentTitle</th><th>ResultPositionRangeStart</th><th>ResultPositionRangeEnd</th><th>ResultCountRangeStart</th><th>ResultCountRangeEnd</th><th>DateCreated</th><th>Created_By_UserID</th><th>DateUpdated</th><th>Updated_By_UserID</th></tr>'."\n";
			while ($TestsRow = mysql_fetch_assoc($TestsRes))
			{
				$RowCounter++;
				if (($RowCounter % 2)==0) $ClassRow='Odd';
				else $ClassRow='Even';
				echo '<tr class="'.$ClassRow.' ClickRow"><td><a href="?Page=Test&amp;Task=Edit&amp;TestID='.$TestsRow['ID'].'">'.$TestsRow['ID'].'</a></td>';
				echo '<td>'.$TestsRow['EnvironmentID'].'</td>';
				echo '<td>'.$TestsRow['Name'].'</td>';
				echo '<td>'.$TestsRow['Category'].'</td>';
				echo '<td>'.$TestsRow['Description'].'</td>';
				echo '<td>'.$TestsRow['Weighting'].'</td>';
				echo '<td>'.$TestsRow['SearchQuery'].'</td>';
				echo '<td>'.$TestsRow['SearchParameters'].'</td>';
				echo '<td>'.$TestsRow['ResultDocumentID'].'</td>';
				echo '<td>'.$TestsRow['ResultDocumentTitle'].'</td>';
				echo '<td>'.$TestsRow['ResultPositionRangeStart'].'</td>';
				echo '<td>'.$TestsRow['ResultPositionRangeEnd'].'</td>';
				echo '<td>'.$TestsRow['ResultCountRangeStart'].'</td>';
				echo '<td>'.$TestsRow['ResultCountRangeEnd'].'</td>';
				echo '<td>'.$TestsRow['DateCreated'].'</td>';
				echo '<td>'.$TestsRow['Created_By_UserID'].'</td>';
				echo '<td>'.$TestsRow['DateUpdated'].'</td>';
				echo '<td>'.$TestsRow['Updated_By_UserID'].'</td>';
			}
			echo '</table>'."\n";
***/
			break;
		default:
			echo '<p class="Error">Access not allowed or the requested action/Environment is unknown.</p>';
		break;
	}
?>