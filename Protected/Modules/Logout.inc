<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	//echo '<div class="TitleLine"><span class="Highlight">'.getWord('Pages', 'LogoutTitle').'</span></div><br />';

	if ($Debug)
	{
		var_dump($_COOKIE);
		var_dump($_SESSION);
	}

	$_SESSION['getWords'] = false;
 	$_SESSION['Language'] = false;
 	$_SESSION['UserID'] = false;
 	$_SESSION['AdminLevel'] = false;

	unset($_SESSION['getWords']);
 	unset($_SESSION['Language']);
	unset($_SESSION['UserID']);
	unset($_SESSION['UserNaam']);
	unset($_SESSION['AdminLevel']);

	echo '<p>Je bent nu uitgelogd. <a href="?Page=Login">Klik hier om opnieuw in te loggen.</a></p>';
	?>

	<script type="text/javascript">window.location.href="?Page=Login";</script>				
	<?php 
	
	if ($Debug)
	{
		var_dump($_COOKIE);
		var_dump($_SESSION);
	}
?>