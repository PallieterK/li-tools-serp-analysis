<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$Task = (empty($_GET['Task']) ? '' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
        echo '<h1>Tests</h1>';
	echo '<div id="NewItem"><a href="?Page=Test&amp;Task=New">Create New Test</a><br /><br /></div>';
	if (empty($Task) || $Task == 'List')
	{
		// Show the search form:
		echo '<form id="UsersSearchForm" action="';
		if (!empty($Page)) { echo '?Page='.$Page; }
		if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
		echo '&amp;Task=List" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
		echo '<table>';
		echo '<tr><td>Environment</td><td><select name="EnvironmentID">';
		echo '<option value="ALL"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'ALL' ? ' selected="selected"' : '').'>search all tests</option>';
		echo '<option value="NULL"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'NULL' ? ' selected="selected"' : '').'>search all-environments tests</option>';
			$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments ORDER BY Name ASC, API_URL ASC, API_Version DESC';
			$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
			{
				echo '<option value="'.$EnvironmentRow['ID'].'"'.(!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == $EnvironmentRow['ID'] ? ' selected="selected"' : '').'>'.$EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'].'</option>';
			}
		echo '</select></td></tr>';
		echo '<tr><td>Name</td><td><input type="text" name="Name"'.(!empty($_POST['Name']) ? ' value="'.htmlspecialchars($_POST['Name']).'"' : '').' /></td></tr>';
		echo '<tr><td>Category</td><td><input type="text" name="Category"'.(!empty($_POST['Category']) ? ' value="'.htmlspecialchars($_POST['Category']).'"' : '').' /></td></tr>';
		echo '<tr><td>Description</td><td><input type="text" name="Description"'.(!empty($_POST['Description']) ? ' value="'.htmlspecialchars($_POST['Description']).'"' : '').' /></td></tr>';
		echo '<tr><td>Priority</td><td>';
			echo '<select name="Weighting"><option value="ALL"'.(!empty($_POST['Weighting']) && $_POST['Weighting'] == 'ALL' ? ' selected="selected"' : '').'>search all tests</option>';
			$WeightingsStr = 'SELECT DISTINCT(Weighting) FROM '.$DataBase['TablePrefix'].'Tests ORDER BY Weighting ASC';
			$WeightingsRes = mysql_query($WeightingsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			while ($WeightingsRow = mysql_fetch_assoc($WeightingsRes))
			{
				echo '<option value="'.$WeightingsRow['Weighting'].'"'.(isset($_POST['Weighting']) && $WeightingsRow['Weighting'] == $_POST['Weighting'] ? ' selected="selected"' : '').'>'.$WeightingsRow['Weighting'].'</option>';
			}
			echo '</select></td></tr>';
		echo '<tr><td>SearchQuery</td><td><input type="text" name="SearchQuery"'.(!empty($_POST['SearchQuery']) ? ' value="'.htmlspecialchars($_POST['SearchQuery']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultDocumentID</td><td><input type="text" name="ResultDocumentID"'.(!empty($_POST['XResultDocumentIDXXX']) ? ' value="'.htmlspecialchars($_POST['ResultDocumentID']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultDocumentTitle</td><td><input type="text" name="ResultDocumentTitle"'.(!empty($_POST['ResultDocumentTitle']) ? ' value="'.htmlspecialchars($_POST['ResultDocumentTitle']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultPositionRangeStart</td><td><input type="text" name="ResultPositionRangeStart"'.(!empty($_POST['ResultPositionRangeStart']) ? ' value="'.htmlspecialchars($_POST['ResultPositionRangeStart']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultPositionRangeEnd</td><td><input type="text" name="ResultPositionRangeEnd"'.(!empty($_POST['ResultPositionRangeEnd']) ? ' value="'.htmlspecialchars($_POST['ResultPositionRangeEnd']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultCountRangeStart</td><td><input type="text" name="ResultCountRangeStart"'.(!empty($_POST['ResultCountRangeStart']) ? ' value="'.htmlspecialchars($_POST['ResultCountRangeStart']).'"' : '').' /></td></tr>';
		echo '<tr><td>ResultCountRangeEnd</td><td><input type="text" name="ResultCountRangeEnd"'.(!empty($_POST['ResultCountRangeEnd']) ? ' value="'.htmlspecialchars($_POST['ResultCountRangeEnd']).'"' : '').' /></td></tr>';
		if (!empty($_SESSION['SpamCheck'])) { $SpamCheck = $_SESSION['SpamCheck']; }
		$_SESSION['SpamCheck'] = rand();
		echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" value="'.getWord('Forms', 'Search').'" name="submit" /></td></tr>';
		echo '</table><br />';
		// Show the list of available Users:
		$TestsStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Tests ';
		if (!empty($_POST) && $SpamCheck == $_POST['SpamCheck'])
		{
			$Where = false;
			$TestsWhereStr = '';
			if (!empty($_POST['EnvironmentID']) && is_numeric($_POST['EnvironmentID']))
			{
				$TestsWhereStr .= "EnvironmentID = '".$_POST['EnvironmentID']."' AND ";
				$Where = true;
			}
			if (!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'NULL')
			{
				$TestsWhereStr .= "EnvironmentID IS NULL ____";
				$Where = true;
			}
			if (!empty($_POST['Name']))
			{
				$TestsWhereStr .= "Name LIKE '%".mysql_real_escape_string($_POST['Name'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['Category']))
			{
				$TestsWhereStr .= "Category LIKE '%".mysql_real_escape_string($_POST['Category'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['Description']))
			{
				$TestsWhereStr .= "Description LIKE '%".mysql_real_escape_string($_POST['Description'])."%' AND ";
				$Where = true;
			}
			if (isset($_POST['Weighting']) && is_numeric($_POST['Weighting']))
			{
				$TestsWhereStr .= "Weighting = ".$_POST['Weighting']." AND ";
				$Where = true;
			}
			if (!empty($_POST['SearchQuery']))
			{
				$TestsWhereStr .= "SearchQuery='".mysql_real_escape_string($_POST['SearchQuery'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultDocumentID']) && is_numeric($_POST['ResultDocumentID']))
			{
				$TestsWhereStr .= "ResultDocumentID = '".mysql_real_escape_string($_POST['ResultDocumentID'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultDocumentTitle']))
			{
				$TestsWhereStr .= "ResultDocumentTitle LIKE '%".mysql_real_escape_string($_POST['ResultDocumentTitle'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultPositionRangeStart']) && is_numeric($_POST['ResultPositionRangeStart']))
			{
				$TestsWhereStr .= "ResultPositionRangeStart = '".mysql_real_escape_string($_POST['ResultPositionRangeStart'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultPositionRangeEnd']) && is_numeric($_POST['ResultPositionRangeEnd']))
			{
				$TestsWhereStr .= "ResultPositionRangeEnd = '".mysql_real_escape_string($_POST['ResultPositionRangeEnd'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultCountRangeStart']) && is_numeric($_POST['ResultCountRangeStart']))
			{
				$TestsWhereStr .= "ResultCountRangeStart = '".mysql_real_escape_string($_POST['ResultCountRangeStart'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['ResultCountRangeEnd']) && is_numeric($_POST['ResultCountRangeEnd']))
			{
				$TestsWhereStr .= "ResultCountRangeEnd = '".mysql_real_escape_string($_POST['ResultCountRangeEnd'])."' AND ";
				$Where = true;
			}

			if ($Where)
			{
				$TestsStr .= 'WHERE '.substr($TestsWhereStr, 0, -4);
			}
		}
		$TestsStr .= 'ORDER BY Name';
		$TestsRes = mysql_query($TestsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$TestsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$TestsNum = mysql_num_rows($TestsRes);
		if (empty($TestsNum))
		{
			echo '<p class="Warning">No tests available in the system.</p>';
		}
		else
		{
			// Use this jQuery to make the whole row clickable:
?>
			<script>
				google.setOnLoadCallback(function()
				{
					$('#TestList tr').click(function()
					{
						var href = $(this).find('a').attr('href');
						if (href)
						{
							window.location = href;
						}
					});
				});
			</script>
<?php
			$RowCounter=0;
			echo '<table id="TestList">'."\n";
			echo '<tr><th>ID</th><th>EnvironmentID</th><th>Name</th><th>Category</th><th>Description</th><th>Weighting</th><th>SearchQuery</th><th>SearchParameters</th><th>ResultDocumentID</th><th>ResultDocumentTitle</th><th>ResultPositionRangeStart</th><th>ResultPositionRangeEnd</th><th>ResultCountRangeStart</th><th>ResultCountRangeEnd</th><th>DateCreated</th><th>Created_By_UserID</th><th>DateUpdated</th><th>Updated_By_UserID</th></tr>'."\n";
			while ($TestsRow = mysql_fetch_assoc($TestsRes))
			{
				$RowCounter++;
				if (($RowCounter % 2)==0) $ClassRow='Odd';
				else $ClassRow='Even';
				echo '<tr class="'.$ClassRow.' ClickRow"><td><a href="?Page=Test&amp;Task=Edit&amp;TestID='.$TestsRow['ID'].'">'.$TestsRow['ID'].'</a></td>';
				echo '<td>'.$TestsRow['EnvironmentID'].'</td>';
				echo '<td>'.$TestsRow['Name'].'</td>';
				echo '<td>'.$TestsRow['Category'].'</td>';
				echo '<td>'.$TestsRow['Description'].'</td>';
				echo '<td>'.$TestsRow['Weighting'].'</td>';
				echo '<td>'.$TestsRow['SearchQuery'].'</td>';
				echo '<td>'.$TestsRow['SearchParameters'].'</td>';
				echo '<td>'.$TestsRow['ResultDocumentID'].'</td>';
				echo '<td>'.$TestsRow['ResultDocumentTitle'].'</td>';
				echo '<td>'.$TestsRow['ResultPositionRangeStart'].'</td>';
				echo '<td>'.$TestsRow['ResultPositionRangeEnd'].'</td>';
				echo '<td>'.$TestsRow['ResultCountRangeStart'].'</td>';
				echo '<td>'.$TestsRow['ResultCountRangeEnd'].'</td>';
				echo '<td>'.$TestsRow['DateCreated'].'</td>';
				echo '<td>'.$TestsRow['Created_By_UserID'].'</td>';
				echo '<td>'.$TestsRow['DateUpdated'].'</td>';
				echo '<td>'.$TestsRow['Updated_By_UserID'].'</td>';
			}
			echo '</table>'."\n";
		}
	}
	else
	{
		echo '<p class="Error">Access not allowed or the requested action/task is unknown.</p>';
	}
?>