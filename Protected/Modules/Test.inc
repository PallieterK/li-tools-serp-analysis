<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	if (empty($_SESSION['UserID']))
	{
		echo '<p><a href="?Page=Login">Login</a></p>';
		exit();
	}

	echo '<h1>Test</h1>';
	$EnvironmentID = (empty($_REQUEST['EnvironmentID']) || !is_numeric($_REQUEST['EnvironmentID']) ? 0 : $_REQUEST['EnvironmentID']); // XSS prevention.
	$TestID = (empty($_REQUEST['TestID']) || !is_numeric($_REQUEST['TestID']) ? 0 : $_REQUEST['TestID']); // XSS prevention.
	$Task = (empty($_GET['Task']) ? 'New' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
	switch ($Task)
	{
		case 'Add':
			// Add the Environment to the database:
			if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
			{
				exit('Spammer?');
			}
			$AddEnvironmentStr = 'INSERT INTO '.$DataBase['TablePrefix'].'Tests SET ';
			if (!empty($_POST['EnvironmentID']) && is_numeric($_POST['EnvironmentID']))
			{
				$AddEnvironmentStr .= 'EnvironmentID='.$_POST['EnvironmentID'].', ';
			}
			if (!empty($_POST['Name']))
			{
				$AddEnvironmentStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
			}
			elseif (!empty($_POST['SearchQuery']))
			{
				$AddEnvironmentStr .= "Name='".mysql_real_escape_string($_POST['SearchQuery'])."', ";
			}
			if (!empty($_POST['Category']))
			{
				$AddEnvironmentStr .= "Category='".mysql_real_escape_string($_POST['Category'])."', ";
			}
			if (!empty($_POST['Description']))
			{
				$AddEnvironmentStr .= "Description='".mysql_real_escape_string($_POST['Description'])."', ";
			}
			if (!empty($_POST['Weighting']))
			{
				$AddEnvironmentStr .= "Weighting='".mysql_real_escape_string($_POST['Weighting'])."', ";
			}
			if (!empty($_POST['SearchQuery']))
			{
				$AddEnvironmentStr .= "SearchQuery='".mysql_real_escape_string($_POST['SearchQuery'])."', ";
			}
			if (!empty($_POST['SearchParameters']))
			{
				$AddEnvironmentStr .= "SearchParameters='".mysql_real_escape_string($_POST['SearchParameters'])."', ";
			}
			if (!empty($_POST['ResultDocumentID']))
			{
				$AddEnvironmentStr .= "ResultDocumentID='".mysql_real_escape_string($_POST['ResultDocumentID'])."', ";
			}
			if (!empty($_POST['ResultDocumentTitle']))
			{
				$AddEnvironmentStr .= "ResultDocumentTitle='".mysql_real_escape_string($_POST['ResultDocumentTitle'])."', ";
			}
			if (!empty($_POST['ResultPositionRangeStart']) && is_numeric($_POST['ResultPositionRangeStart']))
			{
				$AddEnvironmentStr .= 'ResultPositionRangeStart='.$_POST['ResultPositionRangeStart'].', ';
			}
			if (!empty($_POST['ResultPositionRangeEnd']) && is_numeric($_POST['ResultPositionRangeEnd']))
			{
				$AddEnvironmentStr .= 'ResultPositionRangeEnd='.$_POST['ResultPositionRangeEnd'].', ';
			}
			if (!empty($_POST['ResultCountRangeStart']) && is_numeric($_POST['ResultCountRangeStart']))
			{
				$AddEnvironmentStr .= 'ResultCountRangeStart='.$_POST['ResultCountRangeStart'].', ';
			}
			if (!empty($_POST['ResultCountRangeEnd']) && is_numeric($_POST['ResultCountRangeEnd']))
			{
				$AddEnvironmentStr .= 'ResultCountRangeEnd='.$_POST['ResultCountRangeEnd'].', ';
			}
			$AddEnvironmentStr .= "DateCreated='".date('Y-m-d H:i:s', $Time)."', Created_By_UserID=".$_SESSION['UserID'].";";
			$AddEnvironmentRes = mysql_query($AddEnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AddEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
			if ($Debug) { echo 'MySQL Query: '.$AddEnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$TestID = mysql_insert_id();
			echo '<p>De test is opgeslagen (ID: '.$TestID.'). Pas de Test hieronder aan, of <a href="?Page=Test&amp;Task=New">voeg een nieuwe Test toe.</a></p>';
		case 'Edit':
			if (empty($TestID))
			{
				echo '<p class="Error">Edit request failed; no (valid) TestID.</p>';
			}
			else
			{
				// Show the edit form:
				$TestStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Tests WHERE ID='.$TestID;
				$TestRes = mysql_query($TestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$TestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
				if ($Debug) { echo 'MySQL Query: '.$TestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				$TestRow = mysql_fetch_assoc($TestRes);
				echo '<form id="EditEnvironmentForm" action="';
				if (!empty($Page)) { echo '?Page='.$Page; }
				if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
				echo '&amp;Task=Update" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
				echo '<table>';
				echo '<tr><td>Environment</td><td><select name="EnvironmentID">';
				echo '<option value="NULL">all-environments test</option>';
					$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments ORDER BY Name ASC, API_URL ASC, API_Version DESC';
					$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
					if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
					while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
					{
						echo '<option value="'.$EnvironmentRow['ID'].'"';
						if ((!empty($TestRow['EnvironmentID']) AND $EnvironmentRow['ID'] == $TestRow['EnvironmentID']) OR (empty($TestRow['EnvironmentID']) AND $EnvironmentRow['ID'] == $EnvironmentID))
						{
							echo ' selected="selected"';
						}
						echo '>'.$EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'].'</option>'; //  +++ '.$TestRow['EnvironmentID'].'/'.$EnvironmentRow['ID'].'/'.$EnvironmentID.'
					}
				echo '</select></td></tr>';
				echo '<tr><td>Name</td><td><input type="text" name="Name" value="'.htmlspecialchars($TestRow['Name']).'" /></td></tr>';
				echo '<tr><td>Category</td><td><input type="text" name="Category" value="'.htmlspecialchars($TestRow['Category']).'" /></td></tr>';
				echo '<tr><td>Description</td><td><input type="text" name="Description" value="'.htmlspecialchars($TestRow['Description']).'" /></td></tr>';
				echo '<tr><td>Priority</td><td>Existing entries: <select name="WeightingSelector" id="WeightingSelector" onchange="document.getElementById(\'Weighting\').value = this.value">';
					echo '<option value="0">0</option>';
					$WeightingsStr = 'SELECT DISTINCT(Weighting) FROM '.$DataBase['TablePrefix'].'Tests WHERE Weighting != 0 ORDER BY Weighting ASC';
					$WeightingsRes = mysql_query($WeightingsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
					if ($Debug) { echo 'MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
					while ($WeightingsRow = mysql_fetch_assoc($WeightingsRes))
					{
						echo '<option value="'.$WeightingsRow['Weighting'].'"'.($WeightingsRow['Weighting'] == $TestRow['Weighting'] ? ' selected="selected"' : '').'>'.$WeightingsRow['Weighting'].'</option>';
					}
					echo '</select> or add a new entry: <input type="text" name="Weighting" id="Weighting" value="'.htmlspecialchars($TestRow['Weighting']).'" /></td></tr>';
				echo '<tr><td>SearchQuery</td><td><input type="text" name="SearchQuery" value="'.htmlspecialchars($TestRow['SearchQuery']).'" /></td></tr>';
				echo '<tr><td>SearchParameters</td><td><input type="text" name="SearchParameters" value="'.htmlspecialchars($TestRow['SearchParameters']).'" /></td></tr>';
				echo '<tr><td>ResultDocumentID</td><td><input type="text" name="ResultDocumentID" value="'.htmlspecialchars($TestRow['ResultDocumentID']).'" /></td></tr>';
				echo '<tr><td>ResultDocumentTitle</td><td><input type="text" name="ResultDocumentTitle" value="'.htmlspecialchars($TestRow['ResultDocumentTitle']).'" /></td></tr>';
				echo '<tr><td>ResultPositionRangeStart</td><td><input type="text" name="ResultPositionRangeStart" value="'.htmlspecialchars($TestRow['ResultPositionRangeStart']).'" /></td></tr>';
				echo '<tr><td>ResultPositionRangeEnd</td><td><input type="text" name="ResultPositionRangeEnd" value="'.htmlspecialchars($TestRow['ResultPositionRangeEnd']).'" /></td></tr>';
				echo '<tr><td>ResultCountRangeStart</td><td><input type="text" name="ResultCountRangeStart" value="'.htmlspecialchars($TestRow['ResultCountRangeStart']).'" /></td></tr>';
				echo '<tr><td>ResultCountRangeEnd</td><td><input type="text" name="ResultCountRangeEnd" value="'.htmlspecialchars($TestRow['ResultCountRangeEnd']).'" /></td></tr>';
				echo '<tr><td>Date Created</td><td>'.$TestRow['DateCreated'].'</td></tr>';
				echo '<tr><td>Created_By_UserID</td><td>'.$TestRow['Created_By_UserID'].'</td></tr>';
				echo '<tr><td>Last Updated</td><td>'.$TestRow['DateUpdated'].'</td></tr>';
				echo '<tr><td>Last Updated By UserID</td><td>'.$TestRow['Updated_By_UserID'].'</td></tr>';
				$_SESSION['SpamCheck'] = rand();
				echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /><input type="hidden" name="TestID" value="'.$TestRow['ID'].'" /></td><td><input type="submit" name="submit" value="SAVE" />';
				echo ' or <a href="?Page=Session&EnvironmentID=false&SessionDescription=Manual+test+run+for+one+test+on+all+environments.&DryRun=true&IncludeAllEnvironmentsTest=false&TestID='.$TestID.'">run this test (on all environments)</a>.</td></tr>';
				echo '</table>';
			}
			break;
		case 'Update':
			// Update the editted Test:
			if (!empty($_POST['TestID']) && is_numeric($_POST['TestID']))
			{
				if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
				{
					exit('Spammer?');
				}
				// Test:
				$EditTestStr = 'UPDATE '.$DataBase['TablePrefix'].'Tests SET ';
				if (!empty($_POST['EnvironmentID']) && is_numeric($_POST['EnvironmentID']))
				{
					$EditTestStr .= "EnvironmentID='".$_POST['EnvironmentID']."', ";
				}
				if (!empty($_POST['EnvironmentID']) && $_POST['EnvironmentID'] == 'NULL')
				{
					$EditTestStr .= "EnvironmentID = NULL, ";
				}
				if (!empty($_POST['Name']))
				{
					$EditTestStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
				}
				if (!empty($_POST['Category']))
				{
					$EditTestStr .= "Category='".mysql_real_escape_string($_POST['Category'])."', ";
				}
				if (!empty($_POST['Description']))
				{
					$EditTestStr .= "Description='".mysql_real_escape_string($_POST['Description'])."', ";
				}
				if (!empty($_POST['Weighting']))
				{
					$EditTestStr .= "Weighting='".mysql_real_escape_string($_POST['Weighting'])."', ";
				}
				if (!empty($_POST['SearchQuery']))
				{
					$EditTestStr .= "SearchQuery='".mysql_real_escape_string($_POST['SearchQuery'])."', ";
				}
				if (!empty($_POST['SearchParameters']))
				{
					$EditTestStr .= "SearchParameters='".mysql_real_escape_string($_POST['SearchParameters'])."', ";
				}
				if (!empty($_POST['ResultDocumentID']))
				{
					$EditTestStr .= "ResultDocumentID='".mysql_real_escape_string($_POST['ResultDocumentID'])."', ";
				}
				if (!empty($_POST['ResultDocumentTitle']))
				{
					$EditTestStr .= "ResultDocumentTitle='".mysql_real_escape_string($_POST['ResultDocumentTitle'])."', ";
				}
				if (!empty($_POST['ResultPositionRangeStart']))
				{
					$EditTestStr .= "ResultPositionRangeStart='".mysql_real_escape_string($_POST['ResultPositionRangeStart'])."', ";
				}
				if (!empty($_POST['ResultPositionRangeEnd']))
				{
					$EditTestStr .= "ResultPositionRangeEnd='".mysql_real_escape_string($_POST['ResultPositionRangeEnd'])."', ";
				}
				if (!empty($_POST['ResultCountRangeStart']))
				{
					$EditTestStr .= "ResultCountRangeStart='".mysql_real_escape_string($_POST['ResultCountRangeStart'])."', ";
				}
				if (!empty($_POST['ResultCountRangeEnd']))
				{
					$EditTestStr .= "ResultCountRangeEnd='".mysql_real_escape_string($_POST['ResultCountRangeEnd'])."', ";
				}
				$EditTestStr .= "DateUpdated='".date('Y-m-d H:i:s', $Time)."', Updated_By_UserID = ".$_SESSION['UserID']." WHERE ID=".$_POST['TestID'].";";
				$EditTestRes = mysql_query($EditTestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EditTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
				if ($Debug) { echo 'MySQL Query: '.$EditTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				echo '<p>De <a href="?Page=Test&amp;Task=Edit&amp;TestID='.$_POST['TestID'].'">test</a> is aangepast.';
				echo 'Terug naar de <a href="?Page=Admin">admin</a>.</p>';
			}
			else
			{
				echo '<p class="Error">Update request failed; no (valid) EnvironmentID.</p>';
			}
			break;
		case 'New':
			// Show the Add Environment form:
			echo '<form id="AddEnvironmentForm" action="';
			if (!empty($Page)) { echo '?Page='.$Page; }
			if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
			echo '&amp;Task=Add" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
			echo '<table>';
			echo '<tr><td>Environment</td><td><select name="EnvironmentID">';
			echo '<option value="NULL">all-environments test</option>';
				$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments ORDER BY Name ASC, API_URL ASC, API_Version DESC';
				$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
				if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
				{
					echo '<option value="'.$EnvironmentRow['ID'].'"'.($EnvironmentRow['ID'] == $EnvironmentID ? ' selected="selected"' : '').'>';
					echo $EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'];
					echo '</option>';
				}
			echo '</select></td></tr>';
			echo '<tr><td>Name</td><td><input type="text" name="Name" /></td></tr>';
			echo '<tr><td>Category</td><td><input type="text" name="Category" /></td></tr>';
			echo '<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>';
			echo '<tr><td>Priority</td><td>Existing entries: <select name="WeightingSelector" id="WeightingSelector" onchange="document.getElementById(\'Weighting\').value = this.value">';
				echo '<option value="0">0</option>';
				$WeightingsStr = 'SELECT DISTINCT(Weighting) FROM '.$DataBase['TablePrefix'].'Tests WHERE Weighting != 0 ORDER BY Weighting ASC';
				$WeightingsRes = mysql_query($WeightingsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
				if ($Debug) { echo 'MySQL Query: '.$WeightingsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				while ($WeightingsRow = mysql_fetch_assoc($WeightingsRes))
				{
					echo '<option value="'.$WeightingsRow['Weighting'].'">'.$WeightingsRow['Weighting'].'</option>';
				}
				echo '</select> or add a new entry: <input type="text" name="Weighting" id="Weighting" value="0" /></td></tr>';
			echo '<tr><td>SearchQuery</td><td><input type="text" name="SearchQuery" /></td></tr>';
			echo '<tr><td>SearchParameters</td><td><input type="text" name="SearchParameters" /></td></tr>';
			echo '<tr><td>ResultDocumentID</td><td><input type="text" name="ResultDocumentID" /></td></tr>';
			echo '<tr><td>ResultDocumentTitle</td><td><input type="text" name="ResultDocumentTitle" /></td></tr>';
			echo '<tr><td>ResultPositionRangeStart</td><td><input type="text" name="ResultPositionRangeStart" /></td></tr>';
			echo '<tr><td>ResultPositionRangeEnd</td><td><input type="text" name="ResultPositionRangeEnd" /></td></tr>';
			echo '<tr><td>ResultCountRangeStart</td><td><input type="text" name="ResultCountRangeStart" /></td></tr>';
			echo '<tr><td>ResultCountRangeEnd</td><td><input type="text" name="ResultCountRangeEnd" /></td></tr>';
			$_SESSION['SpamCheck'] = rand();
			echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" name="submit" value="'.getWord('Forms', 'Submit').'" /></td></tr>';
			echo '</table>';
			break;
		default:
			echo '<p class="Error">Access not allowed or the requested action/Environment is unknown.</p>';
		break;
	}
?>
