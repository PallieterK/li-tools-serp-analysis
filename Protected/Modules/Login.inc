<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$UID = (empty($_POST['UID']) ? '' : trim(strip_tags(substr($_POST['UID'], 0, 64)))); // XSS prevention.
	$PWD = (empty($_POST['PWD']) ? '' : trim(strip_tags(substr($_POST['PWD'], 0, 64)))); // XSS prevention.

	echo '<h1>Inloggen</h1>';
	
	//echo '<div class="TitleLine"><span class="Highlight">'.getWord('Pages', 'LoginTitle').'</span></div><br />';

	if (!empty($UID) && !empty($PWD))
	{
		// Get the user from the database:
		$UsersStr = "SELECT * FROM ".$DataBase['TablePrefix']."Users WHERE UID='".mysql_real_escape_string($UID)."' AND Status='1'";
		$UsersRes = mysql_query($UsersStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$UsersInj.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$UsersStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$UsersRow = mysql_fetch_assoc($UsersRes);
		// Check if we can grant access:
		if (!empty($UsersRow['PWD']) && $UsersRow['PWD'] == md5($PWD))
		{
			// Update the current session id with a newly generated one (to prevent session hijacking):
			session_regenerate_id();
			$_COOKIE['UID'] = $UsersRow['UID'];
			$_SESSION['UserID'] = $UsersRow['ID'];
			$_SESSION['UserNaam'] = $UsersRow['UID'];

			// Verify if User has admin rights:
			if ($UsersRow['Status'] == 1)
			{
				// User has admin rights:
				$_SESSION['AdminLevel'] = true;
				echo '<p class="Message">You are now logged in as an <a href="?Page=Admin&Action=Dashboard">admin</a>.</p>';
				//echo '<script> window.location.href="?Page=Admin"; </script>';
			}
			else
			{
				// Normal user rights:
				$_SESSION['AdminLevel'] = false;
				echo '<p class="Message">You are now logged in, <a href="?Page=Gebruiker&amp;UserID='.$UsersRow['userid'].'">edit your profile</a>.</p>';
			}
		}
		else
		{
			//echo $PWD.' => '.$UsersRow['PWD'].' = '.md5($PWD);
			echo '<p class="Message">You have typed wrong UID and PWD (or your account is disabled), please <a href="?Page=Login">login</a> again.</p>';
		}
	}
	elseif (empty($_SESSION['UserID']))
	{
		// Show login form:
		echo '<form id="LoginForm" action="'.basename($_SERVER['PHP_SELF']);
		if (!empty($_GET['Page']) && $_GET['Page'] != 'Logout') { echo '?Page='.$_GET['Page']; }
		$Action = (empty($_GET['Action']) ? '' : trim(strip_tags(substr($_GET['Action'], 0, 64)))); // XSS prevention.
		if (!empty($_GET['Page']) && !empty($Action)) { echo '&amp;Action='.$Action; }
		echo '" method="post" accept-charset="utf-8">'; // END: form
		echo '<table border="0" cellpadding="2" cellspacing="2" class="Form">';
		echo '<tr><td>Gebruikersnaam</td><td><input type="text" name="UID"';
		if (!empty($_COOKIE['UID']))
		{
			echo ' value="'.$_COOKIE['UID'].'"';
		}
		echo ' /></td></tr>';
		echo '<tr><td>Wachtwoord</td><td><input type="password" name="PWD" /></td></tr>';
		echo '<tr><td></td><td><input type="submit" class="Submit" value="'.getWord('Forms', 'Login').'" /></td></tr>';
		echo '</table>';
		echo '</form>'; // END: LoginForm
	}
	else
	{
		echo 'You are logged in, <a href="?Page=Logout">logout</a> or go to the <a href="?Page=Admin">admin</a>.';
	}
?>