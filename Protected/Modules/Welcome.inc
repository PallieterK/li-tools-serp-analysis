<?php
/*
			// Temporary code to update all DocCounts:
			$Debug = true;
                        $AllPreviousTestStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Results WHERE DocCount IS NULL';
			$AllPreviousTestRes = mysql_query($AllPreviousTestStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AllPreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$AllPreviousTestStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
                        while ($AllPreviousTestRow = mysql_fetch_assoc($AllPreviousTestRes))
			{
				libxml_use_internal_errors(true);
				$XML = simplexml_load_string($AllPreviousTestRow['Output']);
				if ($XML === false)
				{
					//echo '<p class="Error">The returned XML is not valid, see: ';
					//foreach (libxml_get_errors() as $Error)
					//{
					//	echo '<br />'.$Error->message;
					//}
					//echo '</p>';
					if (!empty($AllPreviousTestRow['DocIDs']))
					{
						echo count(explode(',', $AllPreviousTestRow['DocIDs']));
					}
				}
				else
				{
					$numberOfRecordsObject = $XML->xpath('/searchRetrieveResponse/numberOfRecords');
					$numberOfRecords = intval($numberOfRecordsObject[0]);
					if (!empty($numberOfRecords))
					{
						$ResultStr = 'UPDATE '.$DataBase['TablePrefix'].'Results SET DocCount='.$numberOfRecords.' WHERE ID='.$AllPreviousTestRow['ID'];
						$ResultRes = mysql_query($ResultStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$ResultStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
						if ($Debug) { echo 'MySQL Query: '.$ResultStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
					}
				}
			}
*/
?>
<h2>Welcome</h2>
<p>
	<a href="https://legalintelligence.jira.com/wiki/display/LI/Search+Query+Result+Testing">https://legalintelligence.jira.com/wiki/display/LI/Search+Query+Result+Testing</a>
</p>
<h3>TODO</h3>
<ul>
	<li>a way to test if certain result-sets are identical? like when searching for "0-urencontract" and "0 urencontract"</li>
	<li>use the session ID in the URL (might save some time) get it from the redirect URL before we loop all tests</li>
	<li>sort on NDCG with the SD difference from the last result to the mean</li>
	<li>allow compare of 2 test (from different environments, eg: ACC and PRD)</li>
	<li>allow tests to be for all environments (instead of only 1), to allow for easier comparing between different environments</li>
	<li>allow running of groups of tests (and not all tests for 1 environment at the same time)</li>
	<li>add graph for the average time for tests in a session</li>
</ul>
<h3>Tools</h3>
<ul>
	<li>GDrive Spreadsheet: <a href="https://docs.google.com/spreadsheets/d/1WBK3pAuZLLu0oCw7tDiS4irHW_iFfHLEhsYYwnxBlFc/edit">SERP Analysis Tool - Test Query Selection Support Materials</a></li>
	<li>GDrive Document: <a href="https://docs.google.com/document/d/1oMUfleIplmgsVHwsjXVO5ofdP4l9Hbn0YLMRN8y0Oto/edit">Office Server Setup</a></li>
	<li><a href="/phpMyAdmin">phpMyAdmin</a></li>
</ul>
<h3>Manual to run a test</h3>
<ul>
	<li>Go to the <a href="?Page=Admin&Action=Dashboard">dashboard</a>.</li>
	<li>Under Environments, find the environment you want to run a test for and add a name (description) for that instance of the test.</li>
	<li>Click the start button and wait (a long time) for the system to do a trail run of all the tests for that environment.</li>
	<li>If there are no errors that you want to fix, click (at the bottom) to re-run the test and store it in the database.</li>
	<li></li>
	<li></li>
	<li></li>
</ul>