<?php
	require_once($HomeDir.'Protected/Testers.php');

	if (array_key_exists('FormulaData', $_REQUEST) && strlen($_REQUEST['FormulaData']) > 0)
	{
		echo '<h2>'.$_REQUEST['Formula'].'</h2>';
		$inText = $_REQUEST['FormulaData'];
		$a = explode ("\n", $inText);
		$sp = '';
		foreach ($a as $s)
		{
			$s = trim($s);
			if (strlen($s) > 0)
			{
				if (strlen ($sp) > 0)
				{
					preg_match_all ("|(\d+)|", $sp, $ar);
					$prev = $ar[1];
					
					preg_match_all ("|(\d+)|", $s, $ar);
					$new = $ar[1];
					
					$new_CSV = implode(',', $new);
					$prev_CSV = implode(',', $prev);
					
					if ($_REQUEST['Formula'] == 'NDCGLog')
					{
						$d = NDCGLog($prev_CSV, $new_CSV);
					}
					elseif ($_REQUEST['Formula'] == 'NDCGPow')
					{
						$d = NDCGPow($prev_CSV, $new_CSV);
					}
					elseif ($_REQUEST['Formula'] == 'Spearman')
					{
						$d = Spearman($prev_CSV, $new_CSV);
					}
					
					echo '['.join(',', $prev).'] => ['.join(',', $new).'] : '.$d.'<br />';
				}
				$sp = $s;
			}
		}
	}
	else
	{
		$_REQUEST['FormulaData'] = "1 2\n1 2 3\n2 1 3\n2 3 1";
	}

?>
<br />
<form id="FormulaPlaygroundForm" action="<?php
		if (!empty($Page)) { echo '?Page='.$Page; }
		if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
		?>&amp;Task=Execute" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	<textarea name="FormulaData" cols="42" rows="9"><?php echo $_REQUEST['FormulaData']; ?></textarea><br/>
	<button type="submit" name="Formula" value="NDCGLog">NDCG Log</button> &nbsp;
	<button type="submit" name="Formula" value="NDCGPow" />NDCG Pow</button> &nbsp;
	<button type="submit" name="Formula" value="Spearman" />Spearman</button>
</form>
<br />
<table width="100%">
	<tr>
	<td>
	1 2 3 4 5 6<br/>
	2 3 4 5 6 7<br/>
	2 3 4 5 6<br/>
	2 3 4 5 6 8 9<br/>
	2 3 4 5 6 9 8<br/>
	3 2 4 5 6 8 9<br/>
	9 4 2 5 6 8 3<br/>
	</td>
	<td>
	1 2 3 4<br/>
	1 2 3 4 5<br/>
	1 2 3 4<br/>
	1 2 3<br/>
	1 2 3 6<br/>
	1 2 3<br/>
	1 2 3 5<br/>
	</td>
	<td>
	1 2 3 4 5 6 7 8<br/>
	2 1 3 4 5 6 7 8<br/>
	1 2 4 3 5 6 7 8<br/>
	1 2 3 4 6 5 7 8<br/>
	1 2 3 4 5 6 8 7<br/>
	</td>
	</tr>
</table>