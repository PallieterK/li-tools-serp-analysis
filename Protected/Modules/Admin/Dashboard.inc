<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	$Action = (empty($_GET['Action']) ? '' : trim(strip_tags(substr($_GET['Action'], 0, 64)))); // XSS prevention.
	$EnvironmentID = (empty($_REQUEST['EnvironmentID']) || !is_numeric($_REQUEST['EnvironmentID']) ? 0 : $_REQUEST['EnvironmentID']); // XSS prevention.
	if (!empty($_SESSION['UserID']) && !empty($_SESSION['AdminLevel']))
	{
		echo '<h1>Environments</h1>';
?>
<script type="text/javascript">
	function startSessionRedirect(EnvironmentID, SessionDescription, DryRun, IncludeAllEnvironmentsTest)
	{
		location.href='?Page=Session&EnvironmentID='+EnvironmentID+'&SessionDescription='+encodeURIComponent(SessionDescription)+'&DryRun='+DryRun+'&IncludeAllEnvironmentsTest='+IncludeAllEnvironmentsTest;
	}
</script>
<?php
		echo '<ul>';
		if (!empty($_SESSION['SpamCheck']) && !empty($_POST['SpamCheck']) && $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
		{
			exit('Spammer?');
		}
		$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments WHERE DateEnd IS NULL OR DateEnd > NOW() ORDER BY Name ASC, API_URL ASC, API_Version DESC';
		$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
		{
			echo '<li';
			if (empty($EnvironmentRow['DateEnd']) || (date('Y-m-d') > $EnvironmentRow['DateStart'] && date('Y-m-d') < $EnvironmentRow['DateEnd']))
			{
				echo ' style"background-color: #0F0;"';
			}
			echo '>'.$EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'];
			echo '<br /><a class="AdminLink" href="?Page=Environment&amp;Task=Edit&amp;EnvironmentID='.$EnvironmentRow['ID'].'">Edit</a> or <!--/ <a class="AdminLink" href="?Page=Session&amp;EnvironmentID='.$EnvironmentRow['ID'].'">start</a>--> ';
			echo 'start a new session with this name/description/note: <input type="text" id="SessionDescription-'.$EnvironmentRow['ID'].'" name="SessionDescription" /><button type="button" onclick="startSessionRedirect('.$EnvironmentRow['ID'].', $(\'#SessionDescription-'.$EnvironmentRow['ID'].'\').val(), $(\'#DryRun-'.$EnvironmentRow['ID'].'\').prop(\'checked\'), $(\'#IncludeAllEnvironmentsTest-'.$EnvironmentRow['ID'].'\').prop(\'checked\'));">start</button> <input type="checkbox" id="DryRun-'.$EnvironmentRow['ID'].'" name="DryRun-'.$EnvironmentRow['ID'].'" checked="checked" /> DryRun <input type="checkbox" id="IncludeAllEnvironmentsTest-'.$EnvironmentRow['ID'].'" name="IncludeAllEnvironmentsTest-'.$EnvironmentRow['ID'].'" checked="checked" /> Include the all-environments tests.</li>';
		}
		echo '</ul>';

		echo '<h1>Sessions</h1><ul>';

		echo '<form id="SessionSearchForm" action="';
		if (!empty($Page)) { echo '?Page='.$Page; }
		if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
		echo '&amp;Task=List" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
		echo '<table>';
		echo '<tr><td>Test Results</td><td><select name="SessionsResult"><option value="0"'.(empty($_POST['SessionsResult']) || $_POST['SessionsResult'] == '0' ? ' selected="selected"' : '').'>both</option><option value="YES"'.(!empty($_POST['SessionsResult']) && $_POST['SessionsResult'] == 'YES' ? ' selected="selected"' : '').'>successful</option><option value="NO"'.(!empty($_POST['SessionsResult']) && $_POST['SessionsResult'] == 'NO' ? ' selected="selected"' : '').'>failed</option></select></td></tr>';
		// Search: Environment:
		echo '<tr><td>Environment</td><td><select name="EnvironmentID"><option value="0">all</option>';
			$EnvironmentStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Environments ORDER BY Name ASC, API_URL ASC, API_Version DESC';
			$EnvironmentRes = mysql_query($EnvironmentStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
			if ($Debug) { echo 'MySQL Query: '.$EnvironmentStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			while ($EnvironmentRow = mysql_fetch_assoc($EnvironmentRes))
			{
				echo '<option value="'.$EnvironmentRow['ID'].'"'.($EnvironmentRow['ID'] == $EnvironmentID ? ' selected="selected"' : '').'>';
				echo $EnvironmentRow['Name'].' - '.$EnvironmentRow['API_Version'].' ('.$EnvironmentRow['DateStart'].' - '.$EnvironmentRow['DateEnd'].') ID: '.$EnvironmentRow['ID'].' - '.$EnvironmentRow['Description'];
				echo '</option>';
			}
		echo '</select></td></tr>'; // END: Environment
		if (!empty($_SESSION['SpamCheck'])) { $SpamCheck = $_SESSION['SpamCheck']; }
		$_SESSION['SpamCheck'] = rand();
		echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" value="'.getWord('Forms', 'Search').'" name="submit" /> (shows max 25 results)</td></tr>';
		echo '</table>';
		echo '</form><br />';

		$SessionsStr = 'SELECT Sessions.ID AS SessionsID, Environments.*, Sessions.DateStart AS SessionsDateStart, Sessions.DateEnd AS SessionsDateEnd, Sessions.Result AS SessionsResult, Sessions.Description AS SessionDescription FROM '.$DataBase['TablePrefix'].'Sessions JOIN Environments ON Sessions.EnvironmentID = Environments.ID';
		$SessionsWhereStr = '';
		if (!empty($_POST['SessionsResult']))
		{
			if ($_POST['SessionsResult'] == 'YES')
			{
				$SessionsWhereStr .= ' Sessions.Result = 1 AND';
			}
			elseif ($_POST['SessionsResult'] == 'NO')
			{
				$SessionsWhereStr .= ' Sessions.Result = 0 AND';
			}
			else
			{
				exit('Error');
			}
		}
		if (!empty($EnvironmentID))
		{
			$SessionsWhereStr .= ' Environments.ID = '.$EnvironmentID.' AND';
		}
		if (!empty($SessionsWhereStr))
		{
			$SessionsStr .= ' WHERE '.substr($SessionsWhereStr, 0, -4);
		}
		$SessionsStr .= ' ORDER BY Sessions.DateStart DESC';
		$SessionsRes = mysql_query($SessionsStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$SessionsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$SessionsStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		echo '<ul>';
		while ($SessionsRow = mysql_fetch_assoc($SessionsRes))
		{
			//var_dump($SessionsRow);
			echo '<li>Environment: '.$SessionsRow['Name'].' - '.$SessionsRow['API_Version'].' ('.$SessionsRow['DateStart'].' - '.$SessionsRow['DateEnd'].') ID: '.$SessionsRow['ID'].' - '.$SessionsRow['Description'].'<br />Session: '.$SessionsRow['SessionsDateStart'].' - '.$SessionsRow['SessionsDateEnd'].' ('.$SessionsRow['SessionsResult'].') <a class="AdminLink" href="?Page=Session&amp;Task=View&amp;SessionID='.$SessionsRow['SessionsID'].'">'.($SessionsRow['SessionDescription'] ? $SessionsRow['SessionDescription'] : 'test').'</a></li>';
		}
		echo '</ul>';
	}
	else
	{
		echo '<p>Access not allowed or login was expired.</p>';
		echo '<p>Go to the <a href="'.basename($_SERVER['PHP_SELF']).'?Page=Login">login page</a>.</p>';
	}
?>