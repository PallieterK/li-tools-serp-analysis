<h1>Onderhoud Gebruiker</h1>
<?php
	$Task = (empty($_GET['Task']) ? 'New' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
	$UserID = ((empty($_REQUEST['UserID']) || !is_numeric($_REQUEST['UserID'])) ? 0 : $_REQUEST['UserID']); // XSS prevention.
	switch ($Task)
	{
		case 'Add':
			// Add the User to the database:
			if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
			{
				exit('Spammer?');
			}
			$AddUserStr = 'INSERT INTO '.$DataBase['TablePrefix'].'Users SET ';
			if (!empty($_POST['UID']))
			{
				$AddUserStr .= "UID='".mysql_real_escape_string($_POST['UID'])."', ";
			}
			elseif (empty($_POST['UID']) && !empty($_POST['Email']))
			{
				$AddUserStr .= "UID='".mysql_real_escape_string($_POST['Email'])."', ";
			}
			else
			{
				$AddUserStr .= "UID='".substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ2345678923456789'), 0, 8)."', ";
			}
			if (!empty($_POST['PWD']))
			{
				$AddUserStr .= "PWD='".mysql_real_escape_string(md5($_POST['PWD']))."', ";
			}
			if (!empty($_POST['Name']))
			{
				$AddUserStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
			}
			if (!empty($_POST['Email']))
			{
				$AddUserStr .= "Email='".mysql_real_escape_string($_POST['Email'])."', ";
			}
			if (!empty($_POST['Notes']))
			{
				$AddUserStr .= "Notes='".mysql_real_escape_string($_POST['Notes'])."', ";
			}
			if (!empty($_POST['Status']))
			{
				$AddUserStr .= "Status='".mysql_real_escape_string($_POST['Status'])."', ";
			}
			else
			{
				$AddUserStr .= "Status='nee', ";
			}
			if (!empty($_POST['Role']))
			{
				$AddUserStr .= "Role='".mysql_real_escape_string($_POST['Role'])."', ";
			}
			else
			{
				$AddUserStr .= "Role='student', ";
			}
			$AddUserStr .= "aanmaakdatum='".date('Y-m-d H:i:s', $Time)."', created_by_userid=".$_SESSION['UserID'].";";
			$AddUserRes = mysql_query($AddUserStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$AddUserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
			if ($Debug) { echo 'MySQL Query: '.$AddUserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
			$UserID = mysql_insert_id();
			echo '<p>De gebruiker is opgeslagen (ID: '.$UserID.') met als PWD "'.$_POST['PWD'].'". Pas de gebruiker hieronder aan, of <a href="?Page=Admin&amp;Action=User">voeg een nieuwe gebruiker toe.</a></p>';
		case 'Edit':
			if (empty($UserID))
			{
				echo '<p class="Error">Edit request failed; no (valid) UserID.</p>';
			}
			else
			{
				// Show the edit form:
				$UserStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'Users WHERE userid='.$UserID;
				$UserRes = mysql_query($UserStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$UserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
				if ($Debug) { echo 'MySQL Query: '.$UserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				$UserRow = mysql_fetch_assoc($UserRes);
				echo '<form id="EditUserForm" action="';
				if (!empty($Page)) { echo '?Page='.$Page; }
				if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
				echo '&amp;Task=Update" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
				echo '<table>';
				echo '<tr><td>Gebruikers ID</td><td>'.$UserRow['userid'].'</td></tr>';
				echo '<tr><td>UID (login)</td><td><input type="text" name="UID" value="'.htmlspecialchars($UserRow['UID']).'" /></td></tr>';
				echo '<tr><td>PWD</td><td><input type="text" name="PWD" value="" /> (alleen invullen om te resetten)</td></tr>';
				echo '<tr><td>Name</td><td><input type="text" name="Name" value="'.htmlspecialchars($UserRow['Name']).'" /></td></tr>';
				echo '<tr><td>Email</td><td><input type="text" name="Email" value="'.htmlspecialchars($UserRow['Email']).'" /></td></tr>';
				echo '<tr><td>Notes</td><td><input type="text" name="Notes" value="'.htmlspecialchars($UserRow['Notes']).'" /></td></tr>';
				echo '<tr><td>Role</td><td><select name="Role"><option value="admin"'.($UserRow['Role'] == 'admin' ? ' selected="selected"' : '').'>admin</option><option value="student"'.($UserRow['Role'] == 'student' ? ' selected="selected"' : '').'>student</option><option value="docent"'.($UserRow['Role'] == 'docent' ? ' selected="selected"' : '').'>docent</option></select></td></tr>';
				echo '<tr><td>Status</td><td><select name="Status"><option value="ja"'.($UserRow['Status'] == 'ja' ? ' selected="selected"' : '').'>ja</option><option value="nee"'.($UserRow['Status'] == 'nee' ? ' selected="selected"' : '').'>nee</option></select></td></tr>';
				echo '<tr><td>Aanmaak datum</td><td>'.$UserRow['aanmaakdatum'].'</td></tr>';
				echo '<tr><td>Update datum</td><td>'.$UserRow['updatedate'].'</td></tr>';
				$_SESSION['SpamCheck'] = rand();
				echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /><input type="hidden" name="UserID" value="'.$UserRow['userid'].'" /></td><td><input type="submit" name="submit" value="Opslaan" /></td></tr>';
				echo '</table>';
			}
			break;
		case 'Update':

			// Update the editted User:
			if (!empty($_POST['UserID']) && is_numeric($_POST['UserID']))
			{
				if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
				{
					exit('Spammer?');
				}
				// User:
				$EditUserStr = 'UPDATE '.$DataBase['TablePrefix'].'Users SET ';
				if (!empty($_POST['UID']))
				{
					$EditUserStr .= "UID='".mysql_real_escape_string($_POST['UID'])."', ";
				}
				if (!empty($_POST['PWD']))
				{
					$EditUserStr .= "PWD='".mysql_real_escape_string(md5($_POST['PWD']))."', ";
				}
				if (!empty($_POST['Name']))
				{
					$EditUserStr .= "Name='".mysql_real_escape_string($_POST['Name'])."', ";
				}
				if (!empty($_POST['Email']))
				{
					$EditUserStr .= "Email='".mysql_real_escape_string($_POST['Email'])."', ";
				}
				if (!empty($_POST['Notes']))
				{
					$EditUserStr .= "Notes='".mysql_real_escape_string($_POST['Notes'])."', ";
				}
				if (!empty($_POST['Status']))
				{
					$EditUserStr .= "Status='".mysql_real_escape_string($_POST['Status'])."', ";
				}
				if (!empty($_POST['Role']))
				{
					$EditUserStr .= "Role='".mysql_real_escape_string($_POST['Role'])."', ";
				}
				$EditUserStr .= "updatedate='".date('Y-m-d H:i:s', $Time)."' WHERE userid=".$_POST['UserID'].";";
				$EditUserRes = mysql_query($EditUserStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$EditUserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__));
				if ($Debug) { echo 'MySQL Query: '.$EditUserStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
				echo '<p>De gebruiker is aangepast. ';
				if (!empty($_POST['PWD']))
				{
					echo 'Het nieuwe PWD is '.$_POST['PWD'].'". ';
				}
				echo 'Terug naar de <a href="?Page=Admin">admin</a>.</p>';
				?>
					<script>
						window.location.href="?Page=Admin&Action=Users";
					</script>				
				<?php			
			}
			else
			{
				echo '<p class="Error">Update request failed; no (valid) UserID.</p>';
			}
			break;
		case 'New':
			// Show the Add User form:
			echo '<form id="AddUserForm" action="';
			if (!empty($Page)) { echo '?Page='.$Page; }
			if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
			echo '&amp;Task=Add" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
			echo '<table>';
			echo '<tr><td>LoginName</td><td><input type="text" name="UID" /></td></tr>';
			echo '<tr><td>PWD</td><td><input type="text" name="PWD" value="'.substr(str_shuffle('abcdefghijklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ2345678923456789'), 0, 8).'" /></td></tr>';
			echo '<tr><td>Name</td><td><input type="text" name="Name" /></td></tr>';
			echo '<tr><td>Email</td><td><input type="text" name="Email" /></td></tr>';
			echo '<tr><td>Notes</td><td><input type="text" name="Notes" /></td></tr>';
			echo '<tr><td>Role</td><td><select name="Role"><option value="admin">admin</option><option value="student" selected="selected">student</option><option value="docent">docent</option></select></td></tr>';
			echo '<tr><td>Status</td><td><select name="Status"><option value="ja" selected="selected">ja</option><option value="nee">nee</option></select></td></tr>';
			$_SESSION['SpamCheck'] = rand();
			echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" name="submit" value="'.getWord('Forms', 'Submit').'" /></td></tr>';
			echo '</table>';
			break;
		default:
			echo '<p class="Error">Access not allowed or the requested action/task is unknown.</p>';
		break;
	}
?>