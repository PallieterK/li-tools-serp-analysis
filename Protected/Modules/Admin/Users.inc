<?php
	/*
		GOAL:
			* Link to the Add a User form.
			* Show a list of all the available Users (and facilitate a search mechanism).
			* Link to the User Edit form.
	*/

	$Task = (empty($_GET['Task']) ? '' : trim(strip_tags(substr($_GET['Task'], 0, 64)))); // XSS prevention.
	echo '<div id="NewItem"><a href="?Page=Admin&amp;Action=User">Nieuwe gebruiker toevoegen...</a></div>';
	if (empty($Task) || $Task == 'List')
	{
	
		echo '<h1>Gebruikersoverzicht</h1>'."\n";
		// Show the search form:
		echo '<form id="UsersSearchForm" action="';
		if (!empty($Page)) { echo '?Page='.$Page; }
		if (!empty($Page) && !empty($Action)) { echo '&amp;Action='.$Action; }
		echo '&amp;Task=List" method="post" accept-charset="utf-8" enctype="multipart/form-data">';
		echo '<table>';
		echo '<tr><td>Soort gebruiker</td><td><select name="soort"><option value="0"'.(empty($_POST['soort']) ? ' selected="selected"' : '').'>alle</option><option value="admin"'.(!empty($_POST['soort']) && $_POST['soort'] == 'admin' ? ' selected="selected"' : '').'>admin</option><option value="student"'.(!empty($_POST['soort']) && $_POST['soort'] == 'student' ? ' selected="selected"' : '').'>student</option><option value="docent"'.(!empty($_POST['soort']) && $_POST['soort'] == 'docent' ? ' selected="selected"' : '').'>docent</option></select></td></tr>';
		echo '<tr><td>Naam</td><td><input type="text" name="naam"'.(!empty($_POST['naam']) ? ' value="'.htmlspecialchars($_POST['naam']).'"' : '').' /></td></tr>';
		echo '<tr><td>Email</td><td><input type="text" name="email"'.(!empty($_POST['email']) ? ' value="'.htmlspecialchars($_POST['email']).'"' : '').' /></td></tr>';
		echo '<tr><td>Gebruikersnaam</td><td><input type="text" name="gebruikersnaam"'.(!empty($_POST['gebruikersnaam']) ? ' value="'.htmlspecialchars($_POST['gebruikersnaam']).'"' : '').' /></td></tr>';
		echo '<tr><td>Opmerking</td><td><input type="text" name="opmerkingen"'.(!empty($_POST['opmerkingen']) ? ' value="'.htmlspecialchars($_POST['opmerkingen']).'"' : '').' /></td></tr>';
		echo '<tr><td>Actief</td><td><select name="actief"><option value="0"'.(empty($_POST['actief']) || $_POST['actief'] == '0' ? ' selected="selected"' : '').'>beide</option><option value="ja"'.(!empty($_POST['actief']) && $_POST['actief'] == 'ja' ? ' selected="selected"' : '').'>ja</option><option value="nee"'.(!empty($_POST['actief']) && $_POST['actief'] == 'nee' ? ' selected="selected"' : '').'>nee</option></select></td></tr>';
		// Search: opleidinguser.userid: opleidinguser.opleidingsjaarstudiejaarid => opljaarstudiejaar.opljaarstudiejaarid => studiejaar.studiejaarid, studiejaar.studiejaaroms
/*
		echo '<tr><td>Studie jaar</td><td><select name="StudyYear"><option value="0">alle</option>';
		$StudyYearStr = 'SELECT studiejaar.studiejaarid AS StudyYearID, studiejaar.studiejaaroms AS StudyYearName FROM '.$DataBase['TablePrefix'].'studiejaar '; // TODO: check if this actually has active users associated?
		$StudyYearRes = mysql_query($StudyYearStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$StudyYearStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$StudyYearStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		while ($StudyYearRow = mysql_fetch_assoc($StudyYearRes))
		{
			echo '<option value="'.$StudyYearRow['StudyYearID'].'"'.(!empty($_POST['StudyYearID']) && $_POST['StudyYearID'] == $StudyYearRow['StudyYearID'] ? ' selected="selected"' : '').'>'.$StudyYearRow['StudyYearName'].' (ID: '.$StudyYearRow['StudyYearID'].')</option>';
		}
		echo '</select></td></tr>'; // END: StudyYear
*/
		if (!empty($_SESSION['SpamCheck'])) { $SpamCheck = $_SESSION['SpamCheck']; }
		$_SESSION['SpamCheck'] = rand();
		echo '<tr><td><input type="hidden" name="SpamCheck" value="'.$_SESSION['SpamCheck'].'" /></td><td><input type="submit" value="'.getWord('Forms', 'Search').'" name="submit" /></td></tr>';
		echo '</table>';
		// Show the list of available Users:
		$UsersStr = 'SELECT * FROM '.$DataBase['TablePrefix'].'gebruiker ';
		if (!empty($_POST) && $SpamCheck == $_POST['SpamCheck'])
		{
			if (!empty($_POST['StudyYear']))
			{
				// TODO: Search Users that belong to a: $_POST['StudyYear']
				//$UsersStr .= ' JOIN ........'; // Search: opleidinguser.userid: opleidinguser.opleidingsjaarstudiejaarid => opljaarstudiejaar.opljaarstudiejaarid => studiejaar.studiejaarid, studiejaar.studiejaaroms
			}
			// And add the normal WHERE clause:
			$Where = false;
			$UsersWhereStr = '';
			if (!empty($_POST['soort']))
			{
				$UsersWhereStr .= "soort='".mysql_real_escape_string($_POST['soort'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['naam']))
			{
				$UsersWhereStr .= "naam LIKE '%".mysql_real_escape_string($_POST['naam'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['email']))
			{
				$UsersWhereStr .= "email LIKE '%".mysql_real_escape_string($_POST['email'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['gebruikersnaam']))
			{
				$UsersWhereStr .= "gebruikersnaam='".mysql_real_escape_string($_POST['gebruikersnaam'])."' AND ";
				$Where = true;
			}
			if (!empty($_POST['opmerkingen']))
			{
				$UsersWhereStr .= "opmerkingen LIKE '%".mysql_real_escape_string($_POST['opmerkingen'])."%' AND ";
				$Where = true;
			}
			if (!empty($_POST['actief']))
			{
				$UsersWhereStr .= "actief='".mysql_real_escape_string($_POST['actief'])."' AND ";
				$Where = true;
			}
			if ($Where)
			{
				$UsersStr .= 'WHERE '.substr($UsersWhereStr, 0, -4);
			}
		}
		$UsersStr .= 'ORDER BY naam';
		$UsersRes = mysql_query($UsersStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$UsersStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug) { echo 'MySQL Query: '.$UsersStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; }
		$UsersNum = mysql_num_rows($UsersRes);
		if (empty($UsersNum))
		{
			echo '<p class="Warning">Er zijn nog geen Gebruikers beschikbaar.</p>';
		}
		else
		{
			// Use this jQuery to make the whole row clickable:
?>
			<script>
				google.setOnLoadCallback(function()
				{
					$('#UsersList tr').click(function()
					{
						var href = $(this).find('a').attr('href');
						if (href)
						{
							window.location = href;
						}
					});
				});
			</script>
<?php
			$RowCounter=0;
			echo '<table id="UsersList">'."\n";
			echo '<tr><th>ID</th><th>Soort</th><th>Naam</th><th>Loginnaam</th><th>Email</th><th>Opmerkingen</th><th>Actief</th><th>Aanmaakdatum</th><th>Wijzigingsdatum</th></tr>'."\n";
			while ($UsersRow = mysql_fetch_assoc($UsersRes))
			{
				$RowCounter++;
				if (($RowCounter % 2)==0) $ClassRow='Odd';
				else $ClassRow='Even';
				echo '<tr class="'.$ClassRow.' ClickRow"><td><a href="?Page=Admin&amp;Action=User&amp;Task=Edit&amp;UserID='.$UsersRow['userid'].'">'.$UsersRow['userid'].'</a></td>';
				echo '<td>'.$UsersRow['soort'].'</td>';
				echo '<td>'.$UsersRow['naam'].'</td>';
				echo '<td>'.$UsersRow['gebruikersnaam'].'</td>';
				echo '<td>'.$UsersRow['email'].'</td>';
				echo '<td>'.$UsersRow['opmerkingen'].'</td>';
				echo '<td>'.$UsersRow['actief'].'</td>';
				echo '<td>'.$UsersRow['aanmaakdatum'].'</td>';
				echo '<td>'.$UsersRow['updatedate'].'</td></tr>'."\n";
			}
			echo '</table>'."\n";
		}
	}
	else
	{
		echo '<p class="Error">Access not allowed or the requested action/task is unknown.</p>';
	}
?>