<?php	// Includes all common server and website variables.

	// Set debuging mode:
	$Debug = false; // Used for testing (true=on, false=off). In this mode, additional information/option and all error messages are shown.
	if (!($Debug && !empty($_SERVER['HTTP_HOST']) && false === strpos($_SERVER['HTTP_HOST'], 'www.')))
	{
		// $Debug = false;
	}

	// Set the error reporting level:
	if ($Debug)
	{
		error_reporting(E_ALL);
		ini_set('display_errors', true); // This helps on servers where the PHP error logging happens via a log file instead of displaying it on screen. Turn this off when the site goes live!
	}
	else
	{
		error_reporting(0);
		ini_set('display_errors', false); // Prevents visible output.
	}

	$DomainName				= strtolower($_SERVER['SERVER_NAME']); // The primary domain name (eg: eBrain.nl).

	$RootStr = $_SERVER['DOCUMENT_ROOT'];
	if (substr($RootStr, -1)!='/') { $RootStr .= '/'; } // op sommige servers is de root zonder eindslash
	$wwwDir				= 'SERP_Testing/'; // Path from $SiteURL/ to the main index file (INcluding trailing slash). This is used to rewrite links to the absolute URL path.
	$HomeDir				= $RootStr.$wwwDir; // Absolute path (INcluding the trailing slash).

	if ($DomainName=='192.168.0.203') { 
		$DataBase['Server']	= 'localhost'; // This is almost always: localhost
		$DataBase['Port']		= '3306'; // This is almost always: 3306
		$DataBase['User']		= 'SerpTester'; // Database login account.
		$DataBase['Password']	= 'mvXn4Pw5drT5LFY8'; // Database login password.
		$DataBase['Name']		= 'LI-SERP-Stats'; // Preferably use the CVS name of the project (with the appropriate SwitchCase).
		error_reporting(E_ALL);
		ini_set('display_errors', true);
	} else {
		$DataBase['Server']	= 'localhost'; // This is almost always: localhost
		$DataBase['Port']		= '3306'; // This is almost always: 3306
		$DataBase['User']		= ''; // Database login account.
		$DataBase['Password']	= ''; // Database login password.
		$DataBase['Name']		= ''; // Preferably use the CVS name of the project (with the appropriate SwitchCase).
	}

	$DataBase['TablePrefix']	= ''; // Use the project-initials.
	$DataBase['Engine']		= 'mysql'; // Use a PEAR compatible data source name.


	$SiteURL			= 'http://'.$DomainName.'/';
	$FullSiteURL			= $SiteURL.$wwwDir; // URL path to the main index file (INcluding trailing slash); in most cases this includes the /www/.

	$AddedImagesDir			= $HomeDir.'Uploads/';
	$AddedImagesRelDir		= $FullSiteURL.'Uploads/';

	$Time					= time(); // This is so we have 1 definate common time throughout the whole script.
	$TimeZone				= 'Europe/Amsterdam';

	$DefaultLanguage			= 'EN'; // Set the default language (use the LARGECAP two-letter ISO 639-1 alpha-2 code). Only used if this is not provided by the database.
	$getWordLinks			= false; // Used to auto-create in-site links to the Translations Matrix.

?>
