<?php
	/*
	 * Common Law Copyright 2003 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	// Contains all functionality common to all interactive pages (includes active code & functions).
	require_once(realpath(dirname(__FILE__).'/Included_Raw.inc'));

	// eBrain MailTo Class:
	//require_once(realpath(dirname(__FILE__).'/MailTo.inc'));

	// Start output buffering with gzip content encoding:
	ob_start('ob_gzhandler');

	if (empty($_COOKIE['FirstVisit']))
	{
		$_COOKIE['FirstVisit'] = date('c');
	}
	

$ArrTaxonomieVraag=array("K" => "Kennis", "T" => "Toepassing", "I" => "Inzicht");
$ArrStatusVraag=array("P" => "Publiek", "D" => "Draft", "R" => "Review", "I" => "Intern");
	
	
	function ShowDropdownVeld($ArrList, $ValueSelected, $FieldName) {
		echo '<select name="'.$FieldName.'">'."\n";
		foreach($ArrList as $key => $value){
			echo '<option value="'.$key.'"';
			if ($key==$ValueSelected) echo ' selected="selected"';
			echo '>'.$ArrList[$key]."</option>\n";
		}
		echo "</select>\n";
	}

	function SqlOk($SQLStr, &$result) 
	{
		global $Debug;
		$result = mysql_query($SQLStr) or die ('MySQL Error: '.mysql_error().'<br />MySQL Query: '.$SQLStr.'<br />File: '.__FILE__.' on line: '.(__LINE__ - 1));
		if ($Debug)	{ 
			echo 'MySQL Query: '.$SQLStr.'<br />File: '.__FILE__.' on line: '.(__LINE__)."<br />\n"; 
		}
		if (strpos(substr($SQLStr, 0, 12), "SELECT")===FALSE) return mysql_affected_rows();
		else return mysql_numrows($result);
	}

	function ConstructQueryStr($FieldsCSV) {
		global $_POST;
		$QueryStr='';
		$ArrFields= explode (',' , $FieldsCSV);
		foreach( $ArrFields as $key => $value){
			if (isset($_POST[$value])) $QueryStr.= $value."='".mysql_real_escape_string($_POST[$value])."', ";
			//echo $QueryStr."<br />";
		}
		return $QueryStr;
	}

	function CheckSpammer() {
		global $_SESSION, $_POST;
		if (empty($_SESSION['SpamCheck']) || $_SESSION['SpamCheck'] <> $_POST['SpamCheck'])
		{
			exit('Spammer?');
		}
	}
	
function ToonPaginas($iTotalNrOfRows, $iCurrentPagNr, $iTotalPages) {
	echo '<span class="grijsvlak">Aantal: '.$iTotalNrOfRows."</span>\n";
	//if ($iTotalPages > 1) echo '<span class="grijsvlak">'."Pagina's >></span>\n";
	ShowPageLinks($iTotalNrOfRows, $iCurrentPagNr, $iTotalPages);
	echo '<br><br>';
}

# ShowPageLinks
# Toont bij lijsten links naar alle paginas om zo te kunnen bladeren
#

function ShowPageLinks($iTotalNrOfRows, $iCurrentPage, $ArrParams) {
	global $_GET, $_SERVER, $RowsPerPagina;
	$iTotalPages=ceil($iTotalNrOfRows/$RowsPerPagina);
	$PagNrsTeZien=5; //aantal paginas dat als wordt links wordt getoond links en rechts van het huidige paginanummer
	//echo "iCurrentPage: ".$iCurrentPage;

	if ($iTotalNrOfRows > $RowsPerPagina) {
		if ($iTotalPages > 1) {

			$ParamStr='';
			foreach($ArrParams as $ParamNaam => $ParamWaarde){
				$ParamStr.='&'.$ParamNaam.'='.$ParamWaarde;
			}

			for ($PaginaNr=1; $PaginaNr <= $iTotalPages; $PaginaNr++) {
				//if ($PaginaNr>1) echo "-"; // scheidingsteken

				
				if ($PaginaNr==$iCurrentPage) echo '<span class="currentpagelink">'.$PaginaNr."</span>\n";
				else echo '<a class="otherpagelink" href="'.$_SERVER['PHP_SELF'].'?pagnr='.$PaginaNr.$ParamStr.'">'.$PaginaNr."</a>\n";

				/*
				// skip eventueel na de eerste pagina
				if ($PaginaNr==1 and $iCurrentPage > ($PagNrsTeZien + 2 + 1)) {
				  echo "&nbsp;...&nbsp;";
				  $PaginaNr=$iCurrentPage - $PagNrsTeZien - 1;
				}

				// skip eventueel na de huidige pagina
				if ($PaginaNr==($iCurrentPage + $PagNrsTeZien) and $PaginaNr < $iTotalPages -2 ) {
				  echo "&nbsp;...&nbsp;";
				  $PaginaNr=$iTotalPages - 1;
				}
				*/
			}
		}
	}
}

?>