<?php
	/*
	 * Common Law Copyright 2015 eBrain (c) All rights reserved.
	 *
	 * This is unpublished proprietary code of Pallieter Koopmans.
	 * Your access to it does not give you permission to use it.
	 *
	 */

	header('Content-Type: text/html; charset=utf-8'); // This is a duplicate for the meta-tag (and officially both are not needed, as the XHTML DocType already requires UTF-8 to be used), but many (older) browsers still need it.
	require_once('Protected/Included.inc');
	$Page = (empty($_GET['Page']) ? 'Welcome' : trim(strip_tags(substr($_GET['Page'], 0, 64)))); // XSS prevention.
	// Print the XML-line so PHP does not trip in case the short_open_tag is true:
	echo '<?'.'xml version="1.0" encoding="UTF-8"?>'."\n";

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Legal Intelligence - Search Query Result Testing</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="TODO" />
		<meta name="keywords" content="TODO" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<!--<link rel="stylesheet" href="http://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" type="text/css" />
		<script src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>-->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" type="text/css" />
		<link rel="stylesheet" type="text/css" href="SERP_Testing.css" />
		<!--<script src="https://www.google.com/jsapi" type="text/javascript"></script>
		<script type="text/javascript">
			// Somehow this does not work, so loading it directly, see above.
			//google.load('jquery');
			google.setOnLoadCallback(function()
			{
				// Place init code here instead of $(document).ready()
			});
		</script>-->
                <script type="text/javascript" src="jquery.sparkline.2.1.2.min.js"></script>
	</head>
	<body id="website" style="margin: 0; padding: 0;">
		<div id="container" style="margin: 0; padding: 0;"><!-- begin container -->
		<div id="top"><a href="https://www.legalintelligence.com/"><img style="margin: 10px 0 10px 20px; padding: 0;" src="https://www.legalintelligence.com/Content/Themes/Red/Images/logo.gif" /></a></div>
			<div id="menubalk" style="background-color: #cc0000; height: 60px; padding-left: 20px;">
<?php
				echo '<ul style="margin: 0; padding: 20px 0; list-style-type: none;" id="Menu">';
				echo '<li style="margin: 0 15px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="'.$FullSiteURL.'">Home</a></li>';
				if (!empty($_SESSION['AdminLevel']))
				{
					echo '<li style="margin: 0 15px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Admin&Action=Dashboard">Dashboard</a></li>';
					echo '<li style="margin: 0 15px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Environment">Environments</a></li>';
					echo '<li style="margin: 0 15px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Tests">Tests</a></li>';
					echo '<li style="margin: 0 15px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Playground">Playground</a></li>';
/*
					$AdminModules = scandir(realpath($HomeDir.'/Protected/Modules/'));
					foreach ($AdminModules as $Key => $FileName)
					{
						if (substr($FileName, -4) == '.inc')
						{
							echo '<li style="margin: 0 8px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" class="AdminLink" href="?Page='.substr($FileName, 0, -4).'">'.getWord('Menu', substr($FileName, 0, -4)).'</a></li>';
						}
					}
*/
				}
				if (!empty($_SESSION['UserID']))
				{
					echo '<li style="margin: 0 8px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Logout">Logout</a></li>';
				}
				else
				{
					echo '<li style="margin: 0 8px 0 0; padding: 0; float: left; position: relative;"><a style="color: #fff; text-decoration: none;" href="?Page=Login">Login</a></li>';
				}

?>
				</ul>
			</div>
			<div id="content" style="padding: 20px;">
<?php
				if (!empty($Page) && file_exists(realpath($HomeDir.'/Protected/Modules/'.$Page.'.inc')) && (!empty($_SESSION['UserID']) OR $Page == 'Login'))
				{
					require(realpath($HomeDir.'/Protected/Modules/'.$Page.'.inc'));
				}
				elseif (!empty($Page) && !empty($_SESSION['UserID']))
				{
					echo getWord($Page);
				}
				else
				{
					echo getWord('Welcome');
				}
				echo "\n";
?>
			</div>
			<div id="footer">
				<div style="float: right; width: 230px; font-size: 12px; padding-right: 20px;">
					<p>Copyright &copy; <?php echo date('Y'); ?> &nbsp; <a href="https://www.legalintelligence.com/"><img src="https://www.legalintelligence.com/Content/Images/PRD_LI_footer_logo.png" /></a></p>
				</div>
			</div>
		</div>
	</body>
</html>